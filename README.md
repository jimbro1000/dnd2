# DnD #

Based on the original code from Richard Garriot's dnd game that became the foundation of the Ultima series.
The original basis was written in BASIC, converted to JavaScript for a coding contest run as part of the development of the "Shroud of the Avatar".

### What is this repository for? ###

This repository is the working re-development of the code, from scratch due to significant bugs and problems with the BASIC source.

The code is being rebuilt to the same end-spec using a TDD approach.

### How do I get set up? ###

* The code is a currently a single page application with no server-side operation
* The application runs from the root path with no additional configuration needed
* The intention is to use as few external packages as possible
* Unit tests are composed as Jasmine test specs. To run the tests download the jasmine standalone package. All tests are launched from the `SpecRunner.html` in the test folder.

### How do I play? ###

Load the default page and you should see an almost empty page, just a black frame with a cream fill. The "box" should contain a message detailing the ownership of the code.
![Welcome Message](./documentation/WelcomeMessage.png)

To interact with the game you just need to type. Yes/No questions just need a Y or an N to be typed (with a carriage return). If your typing is not showing on the screen you may have to set the focus on the frame (just click your mouse inside the frame). 
All of the game interaction is through the keyboard. The interface isn't very forgiving when it comes to getting the input right so you need to be precise about what you type.

The welcome message and entry process to the game are true to Richard Garriot's original game, just remember that you don't need instructions and that your name is 'SHAVS'.

### How do I test? ###

There are two sets of specifications, the core components are tested through the SpecRunner.html page in the test folder. 
The game implementation components are teested through the GameSpecRunner.html page in the test_game folder.

### How is it configured? ###

The core of the game is a simple state machine.  
The state machine and the state transitions are described in a json document (in this case game.json). 
The JSON describes some high level values and a list of machine states.
Each state has an identity, a pointer to a function, a flag controlling whether the state machine should pause for input and a list of transition outcomes (identities).
Without a function each state will automatically use the first transition outcome in the list.
It is the job of the function described in the state to decide which transition should be used. This is the logic of the game.
The state machine will run until an unidentified state is encountered or the stop state is reached.

State functions can be re-used across multiple states without any impact provided the necessary transition outcomes are correctly defined in the JSON.

### Who do I talk to? ###

Contact me at jimbro1000@the-lair.demon.co.uk 
