describe("cookie helpers", function () {
    var stateMachine;
    beforeEach(function(done) {
        stateMachine = new StateMachine();
        stateMachine.terminal = new Terminal("terminalConsole", 15, 30);
        stateMachine
            .loadConfiguration("../scripts/game.json")
            .then(
                function (config) {
                    if (!stateMachine.processConfiguration(config)) {
                        throw("Error loading machine: failed to process configuration");
                    }
                    if (stateMachine.validateConfigurationStates()) {
                        stateMachine.ready = true;
                        done();
                    } else {
                        throw("Error loading machine: failed to validate configuration");
                    }
                }
            )
            .catch(function(error) {
                throw("Error in loadMachine: " + error);
            });
    });
    afterEach(function () {
        stateMachine = undefined;
    });

    describe("writeCookie", function () {
        it("exists", function () {
            stateMachine.functionObject.gameStart(stateMachine);
            expect(stateMachine.writeCookie).toBeDefined();
        });
        describe("accepts a key and value as parameters to define the cookie value", function () {
            beforeEach(function () {
                stateMachine.functionObject.gameStart(stateMachine);
            });
            it("accepts a key as the name of the cookie value", function () {
                stateMachine.writeCookie("test");
                expect(document.cookie.indexOf("test")).toBeGreaterThan(-1);
            });
            it("accepts a value as the value of the cookie named", function () {
                var key = "test";
                stateMachine.writeCookie(key, "testValue");
                cookieValue = "";
                key += "=";
                var cookieParts = document.cookie.split(";");
                for (var index=0; index<cookieParts.length; ++index) {
                    var part = cookieParts[index].trim();
                    if (part.indexOf(key) === 0) {
                        cookieValue = part.slice(key.length);
                        break;
                    }
                }
                expect(cookieValue).toBe("testValue");
            });
        });
    });

    describe("readCookie", function () {
        it("exists", function () {
            stateMachine.functionObject.gameStart(stateMachine);
            expect(stateMachine.readCookie).toBeDefined();
        });
        describe("accepts a key as parameter", function () {
            beforeEach(function () {
                stateMachine.functionObject.gameStart(stateMachine);
            });
            it("returns null if the key is not supplied", function () {
                expect(stateMachine.readCookie()).toBeNull();
            });
            it("returns null if the key is not found in the cookie", function () {
                expect(stateMachine.readCookie("not a key")).toBeNull();
            });
            it("returns the value of the cookie key if it is found", function () {
                var d = new Date();
                d.setTime(d.getTime() + 100000);
                document.cookie = "test=value;expires=" + d.toUTCString();
                expect(stateMachine.readCookie("test")).toBe("value");
                d.setTime(d.getTime() - 1000);
                document.cookie = "test=value;expires=" + d.toUTCString();
            });
        });
    });
});