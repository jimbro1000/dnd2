describe("monster", function () {
    it("exists", function () {
        expect(Monster).toBeDefined();
    });
    describe("constructor", function () {
        it("requires name, level, strength, hp, experience and gold as parameters", function () {
            var monster = new Monster("test",1,2,3,4,5);
            expect(monster).toBeDefined();
            expect(monster.name).toBe("test");
            expect(monster.level).toBe(1);
            expect(monster.strength).toBe(2);
            expect(monster.hp).toBe(3);
            expect(monster.experience).toBe(4);
            expect(monster.gold).toBe(5);
        });
        it("accepts an encoded string as a parameter", function () {
            var monster = new Monster("test|1|2|3|4|5");
            expect(monster).toBeDefined();
            expect(monster.name).toBe("test");
            expect(monster.level).toBe(1);
            expect(monster.strength).toBe(2);
            expect(monster.hp).toBe(3);
            expect(monster.experience).toBe(4);
            expect(monster.gold).toBe(5);
        });
    });
    describe("spawn", function () {
        var parent;
        beforeEach(function () {
            parent = new Monster("test",1,2,3,4,5);
        });
        it("generates a new monster object from the parent definition", function () {
            var monster = parent.spawn();
            expect(monster).toBeDefined();
            expect(monster.name).toBe("test");
            expect(monster.level).toBe(1);
            expect(monster.strength).toBe(2);
            expect(monster.hp).toBe(3);
            expect(monster.experience).toBe(4);
            expect(monster.gold).toBe(5);
        });
        it("populates current hp with the default value (hp)", function () {
            var monster = parent.spawn();
            expect(monster.currentHp).toBe(monster.hp);
        });
        it("overrides the stringEncode function of the parent", function () {
            var encoded = parent.spawn().stringEncode();
            expect(encoded).toBe("test|3|5");
        });
    });
    describe("stringEncode", function () {
        it("encodes the monster object as a string", function () {
            expect(new Monster("test",1,2,3,4,5).stringEncode()).toBe("test|1|2|3|4|5");
        });
    });
    describe("teleport", function () {
        var dungeon;
        beforeEach(function() {
            dungeon = new Dungeon(4);
            dungeon.fromString(0,"1111");
            dungeon.fromString(1,"1001");
            dungeon.fromString(2,"1041");
            dungeon.fromString(3,"1111");
        });
        it("exists", function () {
            expect(new Monster("test",1,2,3,4,5).teleport).toBeDefined();
            expect(typeof new Monster("test",1,2,3,4,5).teleport).toEqual("function");
        });
        it("moves the monster to a new position", function () {
            var testMonster = new Monster("test",1,2,3,4,5);
            testMonster.position = { x:1, y:1 };
            testMonster.teleport( 2, 2, dungeon );
            expect(testMonster.position).toEqual( { x:2, y:2 } );
        });
        it("restores the 'ground' under its old position", function () {
            var testMonster = new Monster("test",1,2,3,4,5);
            testMonster.position = { x:1, y:1 };
            testMonster.standingOn = 1;
            testMonster.teleport( 2, 2, dungeon );
            expect(dungeon.lookAt(1,1)).toEqual( 1 );
        });
        it("remember the 'ground' under its new position", function () {
            var testMonster = new Monster("test",1,2,3,4,5);
            testMonster.position = { x:1, y:1 };
            testMonster.teleport( 2, 2, dungeon );
            expect(testMonster.standingOn).toEqual( 4 );
        });
    });
});

describe("monsterManual", function () {
    it("exists", function () {
        expect(MonsterManual).toBeDefined();
    });
    describe("methods", function () {
        var manual;
        beforeEach(function () {
            manual = new MonsterManual();
        });
        describe("add", function () {
            it("places a monster definition in the manual if it is not already in the manual", function () {
                manual.add(new Monster("test",1,2,3,4,5));
                expect(manual.monsters.length).toBe(1);
                expect(manual.monsters[0].name).toBe("test");
                manual.add(new Monster("test",1,2,3,4,5));
                expect(manual.monsters.length).toBe(1);
            });
        });
        describe("stringEncode", function () {
            it("outputs a string encoded version of the manual entry identified by the index parameter", function () {
                manual.add(new Monster("test",1,2,3,4,5));
                expect(manual.stringEncode(0)).toBe("test|1|2|3|4|5");
            });
        });
        describe("stringDecode", function () {
            it("converts an encoded string and adds the resulting monster to the manual", function () {
                manual.stringDecode("test|1|2|3|4|5");
                var monster = manual.fetch("test");
                expect(monster).not.toBeNull();
                expect(monster.name).toBe("test");
                expect(monster.level).toBe(1);
                expect(monster.strength).toBe(2);
                expect(monster.hp).toBe(3);
                expect(monster.experience).toBe(4);
                expect(monster.gold).toBe(5);
            });
        });
        describe("contains", function () {
            beforeEach(function () {
                manual.add(new Monster("test",1,2,3,4,5));
            });
            it("returns true if the given monster name is already in the manual", function () {
                expect(manual.contains("test")).toBe(true);
            });
            it("returns false if the given monster name is not in the manual", function () {
                expect(manual.contains("not a test")).toBe(false);
            });
        });
        describe("fetch", function () {
            beforeEach(function () {
                manual.add(new Monster("test",1,2,3,4,5));
            });
            it("returns a populated monster object if the given monster name is already in the manual", function () {
                expect(manual.fetch("test").name).toBe("test");
            });
            it("returns null if the given monster name is not in the manual", function () {
                expect(manual.fetch("not a test")).toBeNull();
            });
        });
        describe("purge", function () {
            it("removes all entries in the manual", function () {
                manual.add(new Monster("test",1,2,3,4,5));
                expect(manual.monsters.length).toBe(1);
                expect(manual.purge());
                expect(manual.monsters.length).toBe(0);
            });
        });
    });
});