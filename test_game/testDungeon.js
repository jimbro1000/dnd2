describe("Dungeon", function () {
    describe("constructor", function () {
        it("accepts dungeon size (square) as a parameter and generates an empty matrix", function () {
            var test = new Dungeon(25);
            expect(test.size).toBe(25);
            expect(test.grid.length).toBe(test.size);
            expect(test.grid[0].length).toBe(test.size);
            for (var x = 0; x < test.size; ++x) for (var y = 0; y < test.size; ++y) {
                expect(test.grid[x][y]).toBe(test.EMPTY);
            }
        });
    });

    describe("lookAt", function () {
        var dungeon;
        beforeEach(function () {
            dungeon = new Dungeon(25);
            dungeon.grid[0][0] = 1;
        });
        it("returns the contents of the grid at the given coordinate", function () {
            expect(dungeon.lookAt(0, 0)).toBe(1);
        });
    });

    describe("setAt", function () {
        var dungeon;
        beforeEach(function () {
            dungeon = new Dungeon(25);
            dungeon.grid[0][0] = 1;
        });
        it("sets the grid value at the given coordinates to given value", function () {
            dungeon.setAt(1, 1, 2);
            expect(dungeon.lookAt(1, 1)).toBe(2);
        })
    });

    describe("fromString", function () {
        var dungeon;
        beforeEach(function () {
            dungeon = new Dungeon(25);
        });
        it("converts a string of numbers to a column of the grid", function () {
            dungeon.fromString(0, "1234512345123451234512345");
            for (var i = 0; i < 5; ++i) for (var j = 0; j < 5; ++j) {
                expect(dungeon.lookAt(0, i * 5 + j)).toBe(j + 1);
            }
        });
    });

    describe("toString", function () {
        var dungeon;
        beforeEach(function () {
            dungeon = new Dungeon(25);
            dungeon.fromString(0, "1234512345123451234512345");
        });
        it("converts the given grid row into a string of numbers", function () {
            var result = dungeon.toString(0);
            for (var i = 0; i < 25; ++i) {
                var x = result.slice(i, i + 1);
                var y = parseInt(x);
                expect(parseInt(result.slice(i, i + 1))).toBe(dungeon.lookAt(0, i));
            }
        });
    })
});