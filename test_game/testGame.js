describe("Game Functions", function() {
    var stateMachine;
    beforeEach(function(done) {
        stateMachine = new StateMachine();
        stateMachine.terminal = new Terminal("terminalConsole", 15, 30);
        stateMachine
            .loadConfiguration("../scripts/game.json")
            .then(
                function (config) {
                    if (!stateMachine.processConfiguration(config)) {
                        throw("Error loading machine: failed to process configuration");
                    }
                    if (stateMachine.validateConfigurationStates()) {
                        stateMachine.ready = true;
                        done();
                    } else {
                        throw("Error loading machine: failed to validate configuration");
                    }
                }
            )
            .catch(function(error) {
                throw("Error in loadMachine: " + error);
            });
    });
    afterEach(function () {
        stateMachine = undefined;
    });
    describe("Game Exit", function() {
        it("exists", function() {
            expect(stateMachine.functionObject.gameExit).toBeDefined();
        });
        it("terminates the machine processing", function() {
            var result = stateMachine.functionObject.gameExit(stateMachine);
            expect(result).toBe(0);
            expect(stateMachine.configurationStates[0].results[result]).toBe(-1);
        });
    });
    describe("Game Start", function() {
        it("exists", function() {
            expect(stateMachine.functionObject.gameStart).toBeDefined();
        });
        it("progresses to the default next state", function () {
            var result = stateMachine.functionObject.gameStart(stateMachine);
            expect(result).toBe(0);
        });
        it("populates the game item catalogue", function () {
            stateMachine.functionObject.gameStart(stateMachine);
            expect(stateMachine.catalogue).toBeDefined();
        });
        it("populates the game monster manual", function () {
            stateMachine.functionObject.gameStart(stateMachine);
            expect(stateMachine.monsterManual).toBeDefined();
            expect(stateMachine.monsterManual.monsters.length).toBeGreaterThan(0);
        });
    });
    describe("Welcome Message", function() {
        it("exists", function() {
            expect(stateMachine.functionObject.showWelcomeMessage).toBeDefined();
        });
        it("clears the screen and writes the welcome message to the terminal", function () {
            spyOn(stateMachine.terminal,"cls").and.callThrough();
            spyOn(stateMachine.terminal,"printc").and.callThrough();
            stateMachine.functionObject.showWelcomeMessage(stateMachine);
            expect(stateMachine.terminal.cls).toHaveBeenCalled();
            expect(stateMachine.terminal.printc).toHaveBeenCalled();
        });
        it("progresses to the default next state", function () {
            var result = stateMachine.functionObject.gameStart(stateMachine);
            expect(result).toBe(0);
        });
    });
    describe("Prompt user for Instructions", function () {
        it("exists", function () {
            expect(stateMachine.functionObject.askIfInstructionsAreNeeded).toBeDefined();
        });
        it("waits for user input", function () {
            expect(stateMachine.configurationStates[3                  ].waitForInput).toBe(true);
        });
    });
    describe("Handle Request For Instructions", function () {
        it("exists", function () {
            expect(stateMachine.functionObject.handleRequestForInstructions).toBeDefined();
        });
        it("routes to result 0 if the input is 'yes' (and chastise the user)", function () {
            spyOn(stateMachine.terminal,"println").and.callThrough();
            stateMachine.lastInput = "Y";
            var result = stateMachine.functionObject.handleRequestForInstructions(stateMachine);
            expect(result).toBe(0);
            expect(stateMachine.terminal.println).toHaveBeenCalled();
        });
        it("routes to result 1 if the input is 'no'", function () {
            spyOn(stateMachine.terminal,"println").and.callThrough();
            stateMachine.lastInput = "N";
            var result = stateMachine.functionObject.handleRequestForInstructions(stateMachine);
            expect(result).toBe(1);
            expect(stateMachine.terminal.println).not.toHaveBeenCalled();
        });
        it("routes to result 2 if the input is not recognised", function () {
            stateMachine.lastInput = "maybe";
            var result = stateMachine.functionObject.handleRequestForInstructions(stateMachine);
            expect(result).toBe(2);
        });
    });
    describe("promptOldOrNewGame", function () {
        it("exists", function () {
            expect(stateMachine.functionObject.promptOldOrNewGame).toBeDefined();
        });
        it("writes the old or new game prompt to the terminal", function () {
            spyOn(stateMachine.terminal,"println").and.callThrough();
            stateMachine.functionObject.promptOldOrNewGame(stateMachine);
            expect(stateMachine.terminal.println).toHaveBeenCalled();
        });
        it("waits for user input", function () {
            expect(stateMachine.configurationStates[5].waitForInput).toBe(true);
        });
        it("progresses to the default next state", function () {
            var result = stateMachine.functionObject.gameStart(stateMachine);
            expect(result).toBe(0);
        });
    });
    describe("handleOldOrNewGame", function () {
        it("exists", function () {
            expect(stateMachine.functionObject.handleOldOrNewGame).toBeDefined();
        });
        it("routes to result 0 if the input is 'old'", function () {
            stateMachine.lastInput = "OLD";
            var result = stateMachine.functionObject.handleOldOrNewGame(stateMachine);
            expect(result).toBe(0);
        });
        it("routes to result 1 if the input is 'new'", function () {
            stateMachine.lastInput = "NEW";
            var result = stateMachine.functionObject.handleOldOrNewGame(stateMachine);
            expect(result).toBe(1);
        });
        it("routes to result 2 if the input is not 'old' or 'new'", function () {
            spyOn(stateMachine.terminal,"println").and.callThrough();
            stateMachine.lastInput = "OTHER";
            var result = stateMachine.functionObject.handleOldOrNewGame(stateMachine);
            expect(result).toBe(2);
            expect(stateMachine.terminal.println).toHaveBeenCalled();
        });
    });
    describe("promptDungeonNumber",function () {
        it("exists", function () {
            expect(stateMachine.functionObject.promptDungeonNumber).toBeDefined();
        });
        it("writes the dungeon number prompt to the terminal", function () {
            spyOn(stateMachine.terminal,"println").and.callThrough();
            stateMachine.functionObject.promptDungeonNumber(stateMachine);
            expect(stateMachine.terminal.println).toHaveBeenCalled();
        });
        it("waits for user input", function () {
            expect(stateMachine.configurationStates[7].waitForInput).toBe(true);
        });
        it("progresses to the default next state", function () {
            var result = stateMachine.functionObject.promptDungeonNumber(stateMachine);
            expect(result).toBe(0);
        });
    });
    describe("loadSavedGame", function() {
        it("exists", function () {
            expect(stateMachine.functionObject.loadSavedGame).toBeDefined();
        });
        describe("reads state back from cookies", function () {
            beforeEach(function () {
                stateMachine.functionObject.gameStart(stateMachine);
                spyOn(stateMachine,"rnd").and.callFake(function(x) { return x; });
                stateMachine.functionObject.rollNewPlayerStatistics(stateMachine);
                stateMachine.functionObject.defaultDungeon(stateMachine);
                stateMachine.functionObject.spawnMonsters(stateMachine);
                stateMachine.player.attributes[CONSTANTS.PLAYER.CLASS] = "TEST SAVE";
                stateMachine.player.attributes[CONSTANTS.PLAYER.HEALTH] = 10;
                stateMachine.player.inventory.incCount(1);
                stateMachine.functionObject.processSaveGame(stateMachine);
                spyOn(stateMachine,"readCookie").and.callThrough();
            });
            it("reads the contents of the game cookie on the local client", function () {
                stateMachine.functionObject.loadSavedGame(stateMachine);
                expect(stateMachine.readCookie).toHaveBeenCalled();
            });
            it("reads the contents of the player cookie key to restore player state", function () {
                stateMachine.functionObject.loadSavedGame(stateMachine);
                expect(stateMachine.readCookie).toHaveBeenCalledWith("player");
            });
            it("reads the contents of the player.inventory cookie key to restore inventory state", function () {
                stateMachine.functionObject.loadSavedGame(stateMachine);
                expect(stateMachine.readCookie).toHaveBeenCalledWith("player.inventory.0");
            });
            it("reads the contents of the game.catalogue cookie key to restore catalogue state", function () {
                stateMachine.functionObject.loadSavedGame(stateMachine);
                expect(stateMachine.readCookie).toHaveBeenCalledWith("game.catalogue");
            });
            it("reads the contents of the game.dungeon cookie keys to restore catalogue state", function () {
                stateMachine.functionObject.loadSavedGame(stateMachine);
                expect(stateMachine.dungeon.size).toBe(26);
                expect(stateMachine.readCookie).toHaveBeenCalledWith("game.dungeon.0");
            });
            it("reads the contents of the game.monster cookie keys to restore the monster manual", function () {
                stateMachine.functionObject.loadSavedGame(stateMachine);
                expect(stateMachine.monsterManual.monsters.length).toBe(10);
                expect(stateMachine.readCookie).toHaveBeenCalledWith("game.monster");
            });
            it("reads the contents of the game.current.monster cookie keys to restore the live monsters", function () {
                stateMachine.functionObject.loadSavedGame(stateMachine);
                expect(stateMachine.currentMonsters.length).toBe(10);
                expect(stateMachine.readCookie).toHaveBeenCalledWith("game.current.monsters");
            });
            it("reads the contents of the state cookie key to restore state machine state", function () {
                stateMachine.functionObject.loadSavedGame(stateMachine);
                expect(stateMachine.readCookie).toHaveBeenCalledWith("state");
            });
        });
    });
    describe("acceptDungeonNumber", function() {
        it("exists", function () {
            expect(stateMachine.functionObject.acceptDungeonNumber).toBeDefined();
        });
        it("routes to result 1 if the last input is not a number", function () {
            stateMachine.lastInput = "not a number";
            var result = stateMachine.functionObject.acceptDungeonNumber(stateMachine);
            expect(result).toBe(1);
        });
        it("routes to result 1 if the last input is less than 1", function () {
            stateMachine.lastInput = "0";
            var result = stateMachine.functionObject.acceptDungeonNumber(stateMachine);
            expect(result).toBe(1);
        });
        it("routes to result 1 if the last input is greater than 6", function () {
            stateMachine.lastInput = "7";
            var result = stateMachine.functionObject.acceptDungeonNumber(stateMachine);
            expect(result).toBe(1);
        });
        it("routes to result 0 and sets the dungeonNumber property in the state machine", function () {
            stateMachine.lastInput = "6";
            var result = stateMachine.functionObject.acceptDungeonNumber(stateMachine);
            expect(result).toBe(0);
            expect(stateMachine.dungeonNumber).toBe(6);
        });
    });
    describe("loadDungeonNumber", function () {
        it("exists", function () {
            expect(stateMachine.functionObject.loadDungeonNumber).toBeDefined();
        });
        it("loads the default dungeon given a dungeon number of 1", function () {
            spyOn(stateMachine.functionObject, "defaultDungeon").and.callThrough();
            stateMachine.functionObject.gameStart(stateMachine);
            stateMachine.dungeonNumber = 1;
            stateMachine.functionObject.loadDungeonNumber(stateMachine);
            expect(stateMachine.functionObject.defaultDungeon).toHaveBeenCalled();
        })
    });
    describe("askPlayerName", function() {
        it("exists", function () {
            expect(stateMachine.functionObject.askPlayerName).toBeDefined();
        });
        it("writes the player name prompt to the terminal", function () {
            spyOn(stateMachine.terminal,"println").and.callThrough();
            stateMachine.functionObject.askPlayerName(stateMachine);
            expect(stateMachine.terminal.println).toHaveBeenCalled();
        });
        it("waits for user input", function () {
            expect(stateMachine.configurationStates[11].waitForInput).toBe(true);
        });
        it("progresses to the default next state", function () {
            var result = stateMachine.functionObject.askPlayerName(stateMachine);
            expect(result).toBe(0);
        });
    });
    describe("acceptPlayerName", function() {
        it("exists", function () {
            expect(stateMachine.functionObject.acceptPlayerName).toBeDefined();
        });
        it("routes to result 0 if the last input is 'SHAVS", function () {
            stateMachine.lastInput = "SHAVS";
            var result = stateMachine.functionObject.acceptPlayerName(stateMachine);
            expect(result).toBe(0);
        });
        it("routes to result 1 if the last input is not 'SHAVS'", function () {
            spyOn(stateMachine.terminal,"println").and.callThrough();
            stateMachine.lastInput = "other";
            var result = stateMachine.functionObject.acceptPlayerName(stateMachine);
            expect(result).toBe(1);
            expect(stateMachine.terminal.println).toHaveBeenCalled();
        });
    });
    describe("rollNewPlayerStatistics", function() {
        beforeEach(function () {
            stateMachine.functionObject.gameStart(stateMachine);
        });
        it("exists", function () {
            expect(stateMachine.functionObject.rollNewPlayerStatistics).toBeDefined();
        });
        it("populates the player attributes array in the stateMachine", function() {
            stateMachine.functionObject.rollNewPlayerStatistics(stateMachine);
            expect(stateMachine.player.attributes).toBeDefined();
            expect(stateMachine.player.attributes[CONSTANTS.PLAYER.STRENGTH]).toBeDefined();
            expect(stateMachine.player.attributes[CONSTANTS.PLAYER.DEXTERITY]).toBeDefined();
            expect(stateMachine.player.attributes[CONSTANTS.PLAYER.CONSTITUTION]).toBeDefined();
            expect(stateMachine.player.attributes[CONSTANTS.PLAYER.INTELLIGENCE]).toBeDefined();
            expect(stateMachine.player.attributes[CONSTANTS.PLAYER.WISDOM]).toBeDefined();
            expect(stateMachine.player.attributes[CONSTANTS.PLAYER.CHARISMA]).toBeDefined();
            expect(stateMachine.player.attributes[CONSTANTS.PLAYER.GOLD]).toBeDefined();
        });
        it("progresses to the default next state", function () {
            var result = stateMachine.functionObject.rollNewPlayerStatistics(stateMachine);
            expect(result).toBe(0);
        });
    });
    describe("promptPlayerClass", function () {
        it("exists", function () {
            expect(stateMachine.functionObject.promptPlayerClass).toBeDefined();
        });
        it("writes the player class prompt to the terminal", function () {
            spyOn(stateMachine.terminal,"println").and.callThrough();
            stateMachine.functionObject.promptPlayerClass(stateMachine);
            expect(stateMachine.terminal.println).toHaveBeenCalled();
        });
        it("waits for user input", function () {
            expect(stateMachine.configurationStates[15].waitForInput).toBe(true);
        });
        it("progresses to the default next state", function () {
            var result = stateMachine.functionObject.promptPlayerClass(stateMachine);
            expect(result).toBe(0);
        });
    });
    describe("acceptPlayerClass", function () {
        beforeEach(function () {
            stateMachine.functionObject.gameStart(stateMachine);
        });
        it("exists", function () {
            expect(stateMachine.functionObject.acceptPlayerClass).toBeDefined();
        });
        it("gives the player up to 8 HP if FIGHTER is picked", function () {
            spyOn(stateMachine,"rnd").and.callThrough();
            stateMachine.lastInput = "FIGHTER";
            var result = stateMachine.functionObject.acceptPlayerClass(stateMachine);
            expect(stateMachine.rnd).toHaveBeenCalledWith(8);
            expect(stateMachine.player.attributes[0]).toBe("FIGHTER");
            expect(result).toBe(1);
        });
        it("gives the player up to 6 HP if CLERIC is picked", function () {
            spyOn(stateMachine,"rnd").and.callThrough();
            stateMachine.lastInput = "CLERIC";
            var result = stateMachine.functionObject.acceptPlayerClass(stateMachine);
            expect(stateMachine.rnd).toHaveBeenCalledWith(6);
            expect(stateMachine.player.attributes[0]).toBe("CLERIC");
            expect(result).toBe(1);
        });
        it("gives the player up to 4 HP if WIZARD is picked", function () {
            spyOn(stateMachine,"rnd").and.callThrough();
            stateMachine.lastInput = "WIZARD";
            var result = stateMachine.functionObject.acceptPlayerClass(stateMachine);
            expect(stateMachine.rnd).toHaveBeenCalledWith(4);
            expect(stateMachine.player.attributes[0]).toBe("WIZARD");
            expect(result).toBe(1);
        });
        it("routes back to roll stats if the input is 'REROLL'", function () {
            stateMachine.lastInput = "REROLL";
            var result = stateMachine.functionObject.acceptPlayerClass(stateMachine);
            expect(result).toBe(2);
        });
        it("routes back to the prompt if the input is not recognised", function () {
            spyOn(stateMachine.terminal,"println").and.callThrough();
            stateMachine.lastInput = "THIEF";
            var result = stateMachine.functionObject.acceptPlayerClass(stateMachine);
            expect(stateMachine.terminal.println).toHaveBeenCalled();
            expect(result).toBe(0);
        });
    });
    describe("promptUserForShopInventory", function () {
        it("exists", function () {
            expect(stateMachine.functionObject.promptUserForShopInventory).toBeDefined();
        });
        it("writes the user prompt for fast or normal shopping to the terminal", function () {
            spyOn(stateMachine.terminal,"println").and.callThrough();
            stateMachine.functionObject.promptUserForShopInventory(stateMachine);
            expect(stateMachine.terminal.println).toHaveBeenCalled();
        });
        it("waits for user input", function () {
            expect(stateMachine.configurationStates[17].waitForInput).toBe(true);
        });
        it("progresses to the default next state", function () {
            var result = stateMachine.functionObject.promptUserForShopInventory(stateMachine);
            expect(result).toBe(0);
        });
    });
    describe("acceptShopInventory", function () {
        it("exists", function () {
            expect(stateMachine.functionObject.acceptShopInventory).toBeDefined();
        });
        it("routes directly to the shopping prompt if 'FAST' is picked", function () {
            stateMachine.lastInput = "FAST";
            var result = stateMachine.functionObject.acceptShopInventory(stateMachine);
            expect(result).toBe(0);
        });
        it("routes to the shops inventory list if 'NORMAL' is picked", function () {
            stateMachine.lastInput = "NORMAL";
            var result = stateMachine.functionObject.acceptShopInventory(stateMachine);
            expect(result).toBe(1);
        });
        it("routes back to the prompt if the input is not recognised", function () {
            spyOn(stateMachine.terminal,"println").and.callThrough();
            stateMachine.lastInput = "unknown";
            var result = stateMachine.functionObject.acceptShopInventory(stateMachine);
            expect(result).toBe(2);
            expect(stateMachine.terminal.println).toHaveBeenCalled();
        });
    });
    describe("listShopInventory", function () {
        it("exists", function () {
            expect(stateMachine.functionObject.listShopInventory).toBeDefined();
        });
        it("lists all catalogue items with prices", function() {
            spyOn(stateMachine.terminal,"println").and.callThrough();
            stateMachine.functionObject.gameStart(stateMachine);
            var items = stateMachine.catalogue.getSize();
            stateMachine.functionObject.listShopInventory(stateMachine);
            expect(stateMachine.terminal.println).toHaveBeenCalledTimes(items + 2);
        });
    });
    describe("promptUserForItem", function () {
        it("exists", function () {
            expect(stateMachine.functionObject.promptUserForItem).toBeDefined();
        });
        it("displays a prompt to the user", function () {
            spyOn(stateMachine.terminal,"print").and.callThrough();
            stateMachine.functionObject.promptUserForItem(stateMachine);
            expect(stateMachine.terminal.print).toHaveBeenCalledTimes(1);
            expect(stateMachine.terminal.print).toHaveBeenCalledWith(">");
        });
        it("waits for user input", function () {
            expect(stateMachine.configurationStates[20].waitForInput).toBe(true);
        });
    });
    describe("acceptShopItem", function () {
        beforeEach(function () {
            spyOn(stateMachine.terminal,"println").and.callThrough();
        });
        it("exists", function () {
            expect(stateMachine.functionObject.acceptShopItem).toBeDefined();
        });
        it("checks that the lsat input is not a number", function () {
            stateMachine.lastInput = "ben";
            var result = stateMachine.functionObject.acceptShopItem(stateMachine);
            expect(result).toBe(0);
            expect(stateMachine.terminal.println).toHaveBeenCalledWith("YOU MUST INPUT A NUMBER");
        });
        it("checks that the last input is -1", function () {
            stateMachine.lastInput = "-1";
            var result = stateMachine.functionObject.acceptShopItem(stateMachine);
            expect(result).toBe(2);
        });
        it("checks that the last input is -2", function () {
            stateMachine.lastInput = "-2";
            var result = stateMachine.functionObject.acceptShopItem(stateMachine);
            expect(result).toBe(1);
        });
        it("checks that the last input is an invalid negative number", function () {
            stateMachine.lastInput = "-3";
            var result1 = stateMachine.functionObject.acceptShopItem(stateMachine);
            expect(result1).toBe(0);
            expect(stateMachine.terminal.println).toHaveBeenCalledWith("WHAT?");
        });
        it("checks that the last input is zero", function () {
            stateMachine.lastInput = "0";
            var result = stateMachine.functionObject.acceptShopItem(stateMachine);
            expect(result).toBe(0);
            expect(stateMachine.terminal.println).toHaveBeenCalledWith("WHAT?");
        });
        it("checks that the last input is a valid positive number", function () {
            stateMachine.lastInput = "999999";
            stateMachine.functionObject.gameStart(stateMachine);
            var result = stateMachine.functionObject.acceptShopItem(stateMachine);
            expect(result).toBe(0);
            expect(stateMachine.terminal.println).toHaveBeenCalledWith("THAT ITEM DOESN'T EXIST");
        });
        it("checks that the player can use the item", function () {
            stateMachine.lastInput = "1";
            stateMachine.functionObject.gameStart(stateMachine);
            stateMachine.player.attributes[CONSTANTS.PLAYER.CLASS] = "WIZARD";
            var result = stateMachine.functionObject.acceptShopItem(stateMachine);
            expect(result).toBe(0);
            expect(stateMachine.terminal.println).toHaveBeenCalledWith("YOU CAN'T USE THAT");
        });
        it("checks that the player has enough gold to buy the item", function () {
            stateMachine.lastInput = "1";
            stateMachine.functionObject.gameStart(stateMachine);
            stateMachine.player.attributes[CONSTANTS.PLAYER.GOLD] = 1;
            stateMachine.player.attributes[CONSTANTS.PLAYER.CLASS] = "FIGHTER";
            var result = stateMachine.functionObject.acceptShopItem(stateMachine);
            expect(result).toBe(0);
            expect(stateMachine.terminal.println).toHaveBeenCalledWith("COSTS TOO MUCH");
        });
        it("reduces the players gold by the cost of the item", function () {
            stateMachine.lastInput = "1";
            stateMachine.functionObject.gameStart(stateMachine);
            stateMachine.player.attributes[CONSTANTS.PLAYER.GOLD] = 20;
            stateMachine.player.attributes[CONSTANTS.PLAYER.CLASS] = "FIGHTER";
            var result = stateMachine.functionObject.acceptShopItem(stateMachine);
            expect(result).toBe(0);
            expect(stateMachine.player.attributes[CONSTANTS.PLAYER.GOLD]).toBe(10);
        });
        it("increases the count of the selected item", function () {
            stateMachine.lastInput = "1";
            stateMachine.functionObject.gameStart(stateMachine);
            stateMachine.player.attributes[CONSTANTS.PLAYER.GOLD] = 20;
            stateMachine.player.attributes[CONSTANTS.PLAYER.CLASS] = "FIGHTER";
            var result = stateMachine.functionObject.acceptShopItem(stateMachine);
            expect(result).toBe(0);
            expect(stateMachine.player.inventory.fetch(0).count).toBe(1);
        })
    });
    describe("promptShowInventory", function () {
        it("exists", function () {
            expect(stateMachine.functionObject.promptShowInventory).toBeDefined();
        });
        it("displays a prompt to the user to show the inventory", function () {
            spyOn(stateMachine.terminal,"println").and.callThrough();
            stateMachine.functionObject.promptShowInventory(stateMachine);
            expect(stateMachine.terminal.println).toHaveBeenCalled();
        });
        it("waits for user input", function () {
            expect(stateMachine.configurationStates[24].waitForInput).toBe(true);
        });
    });
    describe("acceptShowInventory", function () {
        it("exists", function () {
            expect(stateMachine.functionObject.acceptShowInventory).toBeDefined();
        });
        it("accepts user input of 'N' and routes to option 1", function () {
            stateMachine.lastInput = "N";
            var result = stateMachine.functionObject.acceptShowInventory(stateMachine);
            expect(result).toBe(1);
        });
        it("accepts user input of 'Y' and routes to option 0", function () {
            stateMachine.lastInput = "Y";
            var result = stateMachine.functionObject.acceptShowInventory(stateMachine);
            expect(result).toBe(0);
        });
        it("routes to option 2 for an unrecognised input", function () {
            spyOn(stateMachine.terminal,"println").and.callThrough();
            stateMachine.lastInput = "ben";
            var result = stateMachine.functionObject.acceptShowInventory(stateMachine);
            expect(result).toBe(2);
            expect(stateMachine.terminal.println).toHaveBeenCalled();
        });
    });
    describe("showInventory", function () {
        it("exists",function () {
            expect(stateMachine.functionObject.showInventory).toBeDefined();
        });
        it("lists the contents of the player's inventory", function () {
            spyOn(stateMachine.terminal,"println").and.callThrough();
            stateMachine.functionObject.gameStart(stateMachine);
            stateMachine.player.inventory.incCount(1);
            stateMachine.player.inventory.incCount(7);
            stateMachine.player.inventory.incCount(7);
            stateMachine.functionObject.showInventory(stateMachine);
            expect(stateMachine.terminal.println).toHaveBeenCalledTimes(3);
            expect(stateMachine.terminal.println).toHaveBeenCalledWith("YOU ARE CARRYING:");
        });
        it("tells the player if they aren't carrying anything", function () {
            spyOn(stateMachine.terminal,"println").and.callThrough();
            stateMachine.functionObject.gameStart(stateMachine);
            stateMachine.functionObject.showInventory(stateMachine);
            expect(stateMachine.terminal.println).toHaveBeenCalledTimes(1);
            expect(stateMachine.terminal.println).toHaveBeenCalledWith("YOU AREN'T CARRYING ANYTHING");
        });
    });
    describe("showStats", function () {
        it("exists",function () {
            expect(stateMachine.functionObject.showStats).toBeDefined();
        });
        it("lists the player's class, statistics, health and gold", function () {
            spyOn(stateMachine.terminal,"println").and.callThrough();
            stateMachine.functionObject.gameStart(stateMachine);
            stateMachine.functionObject.rollNewPlayerStatistics(stateMachine);
            stateMachine.player.attributes[CONSTANTS.PLAYER.CLASS] = "TEST CLASS";
            stateMachine.player.attributes[CONSTANTS.PLAYER.HEALTH] = 9999;
            stateMachine.functionObject.showStats(stateMachine);
            expect(stateMachine.terminal.println).toHaveBeenCalledTimes(9);
        });
    });
    describe("gameLoop", function () {
        it("exists", function () {
            expect(stateMachine.functionObject.gameLoop).toBeDefined();
        });
        it("prompts the player for input for next game action", function () {
            spyOn(stateMachine.terminal,"print").and.callThrough();
            stateMachine.functionObject.gameLoop(stateMachine);
            expect(stateMachine.terminal.print).toHaveBeenCalledWith(">");
        });
        it("waits for user input", function () {
            expect(stateMachine.configurationStates[100].waitForInput).toBe(true);
        });
    });
    describe("acceptPlayerInput", function () {
        it("exists", function () {
            expect(stateMachine.functionObject.acceptPlayerInput).toBeDefined();
        });
        it("routes to option 0 if the input is '0'", function () {
            stateMachine.lastInput = "0";
            expect(stateMachine.functionObject.acceptPlayerInput(stateMachine)).toBe(0);
        });
        it("routes to option 1 if the input is '99'", function () {
            spyOn(stateMachine.terminal,"println").and.callThrough();
            stateMachine.lastInput = "99";
            expect(stateMachine.functionObject.acceptPlayerInput(stateMachine)).toBe(1);
        });
        it("routes to option 2 if the input is not recognised", function () {
            spyOn(stateMachine.terminal,"println").and.callThrough();
            stateMachine.lastInput = "bill";
            expect(stateMachine.functionObject.acceptPlayerInput(stateMachine)).toBe(2);
            expect(stateMachine.terminal.println).toHaveBeenCalledWith("WHAT?");
        });
        it("routes to option 3 if the input is '12'", function () {
            stateMachine.lastInput = "12";
            expect(stateMachine.functionObject.acceptPlayerInput(stateMachine)).toBe(3);
        });
        it("routes to option 4 if the input is '1'", function () {
            stateMachine.lastInput = "1";
            expect(stateMachine.functionObject.acceptPlayerInput(stateMachine)).toBe(4);
        });
        it("routes to option 5 if the input is '3'", function () {
            stateMachine.lastInput = "3";
            expect(stateMachine.functionObject.acceptPlayerInput(stateMachine)).toBe(5);
        });
        it("routes to option 6 if the input is '4'", function () {
            stateMachine.lastInput = "4";
            expect(stateMachine.functionObject.acceptPlayerInput(stateMachine)).toBe(6);
        });
        it("routes to option 7 if the input is '5'", function () {
            stateMachine.lastInput = "5";
            expect(stateMachine.functionObject.acceptPlayerInput(stateMachine)).toBe(7);
        });
        it("routes to option 8 if the input is '2'", function () {
            stateMachine.lastInput = "2";
            expect(stateMachine.functionObject.acceptPlayerInput(stateMachine)).toBe(8);
        });
        it("routes to option 9 if the input is '6'", function () {
            stateMachine.lastInput = "6";
            expect(stateMachine.functionObject.acceptPlayerInput(stateMachine)).toBe(9);
        });
        it("routes to option 10 if the input is '7'", function () {
            stateMachine.lastInput = "7";
            expect(stateMachine.functionObject.acceptPlayerInput(stateMachine)).toBe(10);
        });
        it("routes to option 11 if the input is '8'", function () {
            stateMachine.lastInput = "8";
            expect(stateMachine.functionObject.acceptPlayerInput(stateMachine)).toBe(11);
        });
    });
    describe("quitGame", function () {
        it("exists", function () {
            expect(stateMachine.functionObject.quitGame).toBeDefined();
        });
        it("confirms the action and returns the default result", function () {
            spyOn(stateMachine.terminal,"println").and.callThrough();
            expect(stateMachine.functionObject.quitGame(stateMachine)).toBe(0);
            expect(stateMachine.terminal.println).toHaveBeenCalledWith("GOODBYE");
        });
    });
    describe("showCommands", function () {
        it("exists", function () {
            expect(stateMachine.functionObject.showCommands).toBeDefined();
        });
        it("prints a list of available commands to the terinal", function () {
            spyOn(stateMachine.terminal,"println").and.callThrough();
            stateMachine.functionObject.showCommands(stateMachine);
            expect(stateMachine.terminal.println).toHaveBeenCalled();
        })
    });
    // describe("mockCycle", function () {
    //     it("exists", function ()  {
    //         expect(stateMachine.functionObject.mockCycle).toBeDefined();
    //     });
    // });
    describe("promptPlayerMoveDirection", function () {
        it("exists", function () {
            expect(stateMachine.functionObject.promptPlayerMoveDirection).toBeDefined();
        });
        it("prompts the player for a direction to move in", function () {
            spyOn(stateMachine.terminal, "print").and.callThrough();
            spyOn(stateMachine.terminal, "println").and.callThrough();
            stateMachine.functionObject.gameStart(stateMachine);
            stateMachine.functionObject.promptPlayerMoveDirection(stateMachine);
            expect(stateMachine.terminal.println).toHaveBeenCalledWith("DIRECTION (1 = NORTH, 2 = EAST, 3 = SOUTH, 4 = WEST)");
            expect(stateMachine.terminal.print).toHaveBeenCalledWith(">");
        });
        it("waits for user input", function () {
            expect(stateMachine.configurationStates[110].waitForInput).toBe(true);
        });
    });
    describe("acceptPlayerMoveDirection", function () {
        it("exists", function () {
            expect(stateMachine.functionObject.acceptPlayerMoveDirection).toBeDefined();
        });
        it("routes to option 1 if the result is recognised as NORTH", function () {
            stateMachine.lastInput = "1";
            stateMachine.functionObject.gameStart(stateMachine);
            var result = stateMachine.functionObject.acceptPlayerMoveDirection(stateMachine);
            expect(result).toBe(1);
        });
        it("sets the player.move value to 1 if the input is NORTH", function () {
            stateMachine.lastInput = "1";
            stateMachine.functionObject.gameStart(stateMachine);
            stateMachine.functionObject.acceptPlayerMoveDirection(stateMachine);
            expect(stateMachine.player.move).toBe(1);
        });
        it("routes to option 1 if the result is recognised as EAST", function () {
            stateMachine.lastInput = "2";
            stateMachine.functionObject.gameStart(stateMachine);
            var result = stateMachine.functionObject.acceptPlayerMoveDirection(stateMachine);
            expect(result).toBe(1);
        });
        it("sets the player.move value to 2 if the input is EAST", function () {
            stateMachine.lastInput = "2";
            stateMachine.functionObject.gameStart(stateMachine);
            stateMachine.functionObject.acceptPlayerMoveDirection(stateMachine);
            expect(stateMachine.player.move).toBe(2);
        });
        it("routes to option 1 if the result is recognised as SOUTH", function () {
            stateMachine.lastInput = "3";
            stateMachine.functionObject.gameStart(stateMachine);
            var result = stateMachine.functionObject.acceptPlayerMoveDirection(stateMachine);
            expect(result).toBe(1);
        });
        it("sets the player.move value to 3 if the input is SOUTH", function () {
            stateMachine.lastInput = "3";
            stateMachine.functionObject.gameStart(stateMachine);
            stateMachine.functionObject.acceptPlayerMoveDirection(stateMachine);
            expect(stateMachine.player.move).toBe(3);
        });
        it("routes to option 1 if the result is recognised as WEST", function () {
            stateMachine.lastInput = "4";
            stateMachine.functionObject.gameStart(stateMachine);
            var result = stateMachine.functionObject.acceptPlayerMoveDirection(stateMachine);
            expect(result).toBe(1);
        });
        it("sets the player.move value to 4 if the input is WEST", function () {
            stateMachine.lastInput = "4";
            stateMachine.functionObject.gameStart(stateMachine);
            stateMachine.functionObject.acceptPlayerMoveDirection(stateMachine);
            expect(stateMachine.player.move).toBe(4);
        });
        it("routes to option 0 if the result is unrecognised", function () {
            spyOn(stateMachine.terminal, "println").and.callThrough();
            stateMachine.lastInput = "ben";
            stateMachine.functionObject.gameStart(stateMachine);
            var result = stateMachine.functionObject.acceptPlayerMoveDirection(stateMachine);
            expect(stateMachine.terminal.println).toHaveBeenCalledWith("WHAT?");
            expect(result).toBe(0);
        });
        it("sets player.move to undefined if the result is unrecognised", function () {
            stateMachine.lastInput = "ben";
            stateMachine.functionObject.gameStart(stateMachine);
            stateMachine.functionObject.acceptPlayerMoveDirection(stateMachine);
            expect(stateMachine.player.move).not.toBeDefined();
        });
    });
    describe("processPlayerMove", function () {
        beforeEach(function () {
            stateMachine.functionObject.gameStart(stateMachine);
            stateMachine.dungeonNumber = 1;
            stateMachine.functionObject.defaultDungeon(stateMachine);
        });
        it("exists", function () {
            expect(stateMachine.functionObject.processPlayerMove).toBeDefined();
        });
        it("checks the dungeon grid in the direction the player wants to move", function () {
            spyOn(stateMachine.dungeon, "lookAt").and.callThrough();
            stateMachine.player.move = 1;
            stateMachine.functionObject.processPlayerMove(stateMachine);
            expect(stateMachine.dungeon.lookAt).toHaveBeenCalled();
        });
        it("blocks the move if the way is not clear in the chosen direction", function () {
            spyOn(stateMachine.terminal, "println").and.callThrough();
            stateMachine.player.move = 1;
            stateMachine.functionObject.processPlayerMove(stateMachine);
            expect(stateMachine.player.position.x).toBe(1);
            expect(stateMachine.player.position.y).toBe(1);
            expect(stateMachine.terminal.println).toHaveBeenCalled();
        });
        it("moves the player if the way is clear in the chosen direction", function () {
            stateMachine.player.move = 2;
            stateMachine.functionObject.processPlayerMove(stateMachine);
            expect(stateMachine.player.position.x).toBe(2);
            expect(stateMachine.player.position.y).toBe(1);
        });
    });
    describe("processPlayerSearch", function () {
        beforeEach(function () {
            stateMachine.functionObject.gameStart(stateMachine);
            stateMachine.functionObject.defaultDungeon(stateMachine);
            stateMachine.functionObject.rollNewPlayerStatistics(stateMachine);
            spyOn(stateMachine.terminal, "println").and.callThrough();
        });
        it("exists", function () {
            expect(stateMachine.functionObject.processPlayerSearch).toBeDefined();
        });
        it("examines the area surrounding the players position", function () {
            spyOn(stateMachine,"rnd").and.callFake(function(x) { return x; });
            spyOn(stateMachine.dungeon, "lookAt").and.callThrough();
            stateMachine.functionObject.processPlayerSearch(stateMachine);
            expect(stateMachine.dungeon.lookAt).toHaveBeenCalledTimes(8);
        });
        it("it identifies secret doors", function () {
            spyOn(stateMachine,"rnd").and.callFake(function(x) { return x; });
            stateMachine.player.position.y = 11;
            stateMachine.functionObject.processPlayerSearch(stateMachine);
            expect(stateMachine.terminal.println).toHaveBeenCalledWith("FOUND A DOOR AT X:1 Y:12");
        });
        it("it identifies hidden traps", function () {
            spyOn(stateMachine,"rnd").and.callFake(function(x) { return x; });
            stateMachine.player.position.y = 17;
            stateMachine.functionObject.processPlayerSearch(stateMachine);
            expect(stateMachine.terminal.println).toHaveBeenCalledWith("FOUND A TRAP AT X:2 Y:17");
        });
        it("fails to find anything if the player is unlucky", function () {
            spyOn(stateMachine,"rnd").and.callFake(function() { return 0; });
            stateMachine.player.position.x = 19;
            stateMachine.player.position.y = 9;
            stateMachine.functionObject.processPlayerSearch(stateMachine);
            expect(stateMachine.terminal.println).toHaveBeenCalledWith("DIDN'T FIND ANYTHING");
        });
    });
    describe("promptPlayerForWeapon", function () {
        beforeEach(function () {
            spyOn(stateMachine.terminal, "println").and.callThrough();
        });
        it("exists", function() {
            expect(stateMachine.functionObject.promptPlayerForWeapon).toBeDefined();
        });
        it("waits for user input", function () {
            expect(stateMachine.configurationStates[130].waitForInput).toBe(true);
        });
        it("prompts the player to enter the weapon to use", function () {
            stateMachine.functionObject.promptPlayerForWeapon(stateMachine);
            expect(stateMachine.terminal.println).toHaveBeenCalledWith("WHICH WEAPON DO YOU WANT TO USE?");
        });
        it("routes to the default outcome", function () {
            expect(stateMachine.functionObject.promptPlayerForWeapon(stateMachine)).toBe(0);
        });
    });
    describe("acceptPlayerWeaponChoice", function () {
        beforeEach(function () {
            spyOn(stateMachine.terminal, "println").and.callThrough();
        });
        it("exists", function() {
            expect(stateMachine.functionObject.acceptPlayerWeaponChoice).toBeDefined();
        });
        it("routes to outcome zero if the last input is not a number", function () {
            stateMachine.lastInput = "ben";
            expect(stateMachine.functionObject.acceptPlayerWeaponChoice(stateMachine)).toBe(0);
        });
        it("tells the player if the last input is not a number", function () {
            stateMachine.lastInput = "ben";
            stateMachine.functionObject.acceptPlayerWeaponChoice(stateMachine);
            expect(stateMachine.terminal.println).toHaveBeenCalledWith("WHAT?");
        });
        it("routes to outcome zero if the last input is a number zero or less", function () {
            stateMachine.lastInput = "0";
            expect(stateMachine.functionObject.acceptPlayerWeaponChoice(stateMachine)).toBe(0);
        });
        it("tells the player if the last input is a number zero or less", function () {
            stateMachine.lastInput = "0";
            stateMachine.functionObject.acceptPlayerWeaponChoice(stateMachine);
            expect(stateMachine.terminal.println).toHaveBeenCalledWith("WHAT?");
        });
        it("tells the player if the last input is a number greater than the maximum item value", function () {
            stateMachine.functionObject.gameStart(stateMachine);
            stateMachine.lastInput = "" + (stateMachine.catalogue.getSize() + 2);
            stateMachine.functionObject.acceptPlayerWeaponChoice(stateMachine);
            expect(stateMachine.terminal.println).toHaveBeenCalledWith("WHAT?");
        });
        it("routes to outcome zero if the last input is a number greater than the maximum item value", function () {
            stateMachine.functionObject.gameStart(stateMachine);
            stateMachine.lastInput = "" + (stateMachine.catalogue.getSize() + 2);
            expect(stateMachine.functionObject.acceptPlayerWeaponChoice(stateMachine)).toBe(0);
        });
        it("tells the player if the chosen weapon is not being carried", function () {
            stateMachine.functionObject.gameStart(stateMachine);
            stateMachine.lastInput = "1";
            stateMachine.functionObject.acceptPlayerWeaponChoice(stateMachine);
            expect(stateMachine.terminal.println).toHaveBeenCalledWith("YOU AREN'T CARRYING ANY");
        });
        it("routes to outcome zero if the chosen weapon is not being carried", function () {
            stateMachine.functionObject.gameStart(stateMachine);
            stateMachine.lastInput = "1";
            expect(stateMachine.functionObject.acceptPlayerWeaponChoice(stateMachine)).toBe(0);
        });
        it("sets the state variable weaponChoice to the value of last input if the chosen weapon is equipable", function () {
            stateMachine.functionObject.gameStart(stateMachine);
            stateMachine.player.inventory.incCount(0);
            stateMachine.lastInput = "1";
            stateMachine.functionObject.acceptPlayerWeaponChoice(stateMachine);
            expect(stateMachine.weaponChoice).toBe(1);
        });
        it("routes to outcome 1 if the chosen weapon is equipable", function () {
            stateMachine.functionObject.gameStart(stateMachine);
            stateMachine.player.inventory.incCount(0);
            stateMachine.lastInput = "1";
            expect(stateMachine.functionObject.acceptPlayerWeaponChoice(stateMachine)).toBe(1);
        });
    });
    describe("processPlayerWeaponChoice", function () {
        beforeEach(function () {
            spyOn(stateMachine.terminal, "println").and.callThrough();
        });
        it("exists", function() {
            expect(stateMachine.functionObject.processPlayerWeaponChoice).toBeDefined();
        });
        it("equips the chosen weapon if it is carried", function () {
            stateMachine.functionObject.gameStart(stateMachine);
            stateMachine.player.inventory.incCount(0);
            stateMachine.weaponChoice = 1;
            stateMachine.functionObject.processPlayerWeaponChoice(stateMachine);
            expect(stateMachine.player.weapon).toBe(0);
        });
        it("confirms the action to the player", function () {
            stateMachine.functionObject.gameStart(stateMachine);
            stateMachine.player.inventory.incCount(0);
            stateMachine.weaponChoice = 1;
            stateMachine.functionObject.processPlayerWeaponChoice(stateMachine);
            expect(stateMachine.terminal.println).toHaveBeenCalledWith("OK YOU ARE NOW HOLDING A SWORD");
        });
    });
    describe("processPlayerLook", function () {
        beforeEach(function () {
            spyOn(stateMachine.terminal, "println").and.callThrough();
        });
        it("exists", function () {
            expect(stateMachine.functionObject.processPlayerLook).toBeDefined();
        });
        it("reports back the immediate visible surroundings of the player", function () {
            stateMachine.functionObject.gameStart(stateMachine);
            stateMachine.functionObject.defaultDungeon(stateMachine);
            stateMachine.player.position = { x: 3, y: 3 };
            stateMachine.functionObject.processPlayerLook(stateMachine);
            expect(stateMachine.terminal.println).toHaveBeenCalledTimes(11);
        });
        it("places an '@' at the centre of the view to show the player", function () {
            stateMachine.functionObject.gameStart(stateMachine);
            stateMachine.functionObject.defaultDungeon(stateMachine);
            stateMachine.player.position = { x: 3, y: 3 };
            stateMachine.functionObject.processPlayerLook(stateMachine);
            expect(stateMachine.terminal.println).toHaveBeenCalledWith("01101@16101");
        });
        it("obscures hidden map items (traps and doors)", function () {
            stateMachine.functionObject.gameStart(stateMachine);
            stateMachine.functionObject.defaultDungeon(stateMachine);
            stateMachine.player.position = { x: 3, y: 3 };
            stateMachine.functionObject.processPlayerLook(stateMachine);
            expect(stateMachine.terminal.println).toHaveBeenCalledWith("01101@16101");
            expect(stateMachine.terminal.println).toHaveBeenCalledWith("01101010101");
        });
        it("returns the default response route", function () {
            stateMachine.functionObject.gameStart(stateMachine);
            stateMachine.functionObject.defaultDungeon(stateMachine);
            stateMachine.player.position = { x: 3, y: 3 };
            expect(stateMachine.functionObject.processPlayerLook(stateMachine)).toBe(0);
        });
    });
    describe("processSaveGame", function () {
        beforeEach(function () {
            stateMachine.functionObject.gameStart(stateMachine);
            spyOn(stateMachine,"rnd").and.callFake(function (x) { return x; });
            spyOn(stateMachine,"writeCookie").and.stub();
        });
        it("exists", function () {
            expect(stateMachine.functionObject.processSaveGame).toBeDefined();
        });
        describe("saves the game state to a cookie on the local client", function () {
            beforeEach(function () {
                stateMachine.functionObject.defaultDungeon(stateMachine);
                stateMachine.functionObject.spawnMonsters(stateMachine);
                stateMachine.functionObject.rollNewPlayerStatistics(stateMachine);
                stateMachine.player.attributes[CONSTANTS.PLAYER.CLASS] = "TEST SAVE";
                stateMachine.player.attributes[CONSTANTS.PLAYER.HEALTH] = 10;
                stateMachine.player.inventory.incCount(1);
            });
            it("writes player information to the player cookie key", function () {
                stateMachine.functionObject.processSaveGame(stateMachine);
                expect(stateMachine.writeCookie).toHaveBeenCalledWith("player","TEST SAVE|21|21|21|21|21|21|315|10");
            });
            it("writes player inventory information to the player.inventory cookie key", function () {
                stateMachine.functionObject.processSaveGame(stateMachine);
                expect(stateMachine.writeCookie).toHaveBeenCalledWith("player.inventory","0|1|0|0|0|0|0|0|0|0|0|0|0|0|0");
            });
            it("writes catalogue information to the game.catalogue set of keys", function () {
                stateMachine.functionObject.processSaveGame(stateMachine);
                expect(stateMachine.writeCookie).toHaveBeenCalledWith("game.catalogue","15");
                expect(stateMachine.writeCookie).toHaveBeenCalledWith("game.catalogue.0","10|SWORD|true|false|false|null|2|0.45454545454545453|0.6666666666666666|FIGHTER");
            });
            it("writes dungeon information to the game.dungeon set of keys", function () {
                stateMachine.functionObject.processSaveGame(stateMachine);
                expect(stateMachine.writeCookie).toHaveBeenCalledWith("game.dungeon", "26");
                expect(stateMachine.writeCookie).toHaveBeenCalledWith("game.dungeon.1","10000000400030161000100001");
            });
            it("writes game state information to the state cookie key", function () {
                stateMachine.functionObject.processSaveGame(stateMachine);
                expect(stateMachine.writeCookie).toHaveBeenCalledWith("state","1");
            });
            it("writes the number of entries in the monster manual to the game.monster key", function () {
                stateMachine.functionObject.processSaveGame(stateMachine);
                expect(stateMachine.writeCookie).toHaveBeenCalledWith("game.monster", "10");
            });
            it("writes each monster definition to a numbered game.monster.n key", function () {
                stateMachine.functionObject.processSaveGame(stateMachine);
                expect(stateMachine.writeCookie).toHaveBeenCalledWith("game.monster.1", "GOBLIN|2|13|24|1|600");
            });
            it("writes the number of entries in the current monsters array to the game.current.monsters key", function () {
                stateMachine.functionObject.processSaveGame(stateMachine);
                expect(stateMachine.writeCookie).toHaveBeenCalledWith("game.current.monsters", "10");
            });
            it("writes each living monster definition to a numbered game.current.monster.n key", function () {
                stateMachine.functionObject.processSaveGame(stateMachine);
                expect(stateMachine.writeCookie).toHaveBeenCalledWith("game.current.monster.1", "GOBLIN|24|600");
            });
        });
    });
    describe("promptUserForReset", function () {
        it("exists", function () {
            expect(stateMachine.functionObject.promptUserForReset).toBeDefined();
        });
        it("returns the default route response", function () {
            expect(stateMachine.functionObject.promptUserForReset(stateMachine)).toBe(0);
        });
        it("prompts the user to choose a reset or not", function () {
            spyOn(stateMachine.terminal,"println").and.callThrough();
            stateMachine.functionObject.promptUserForReset(stateMachine);
            expect(stateMachine.terminal.println).toHaveBeenCalledWith("RESET?");
        });
        it("waits for user input", function () {
            expect(stateMachine.configurationStates[22].waitForInput).toBe(true);
        });
    });
    describe("acceptReset", function () {
        it("exists", function () {
            expect(stateMachine.functionObject.acceptReset).toBeDefined();
        });
        it("routes to response 0 if 'Y' is last input", function () {
            stateMachine.lastInput = "Y";
            expect(stateMachine.functionObject.acceptReset(stateMachine)).toBe(0);
        });
        it("routes to response 0 in 'N' is last input", function () {
            stateMachine.lastInput = "N";
            expect(stateMachine.functionObject.acceptReset(stateMachine)).toBe(0);
        });it("routes to response 1 if last input is not recognised", function () {
            stateMachine.lastInput = "bill";
            expect(stateMachine.functionObject.acceptReset(stateMachine)).toBe(1);
        });
    });
    describe("populateManual)", function () {
        it("exists", function () {
            expect(stateMachine.functionObject.populateManual).toBeDefined();
        });
        it("populates an empty manual with monster entries", function () {
            stateMachine.monsterManual = new MonsterManual();
            stateMachine.functionObject.populateManual(stateMachine);
            expect(stateMachine.monsterManual.monsters.length).toBe(10);
        });
    });
    describe("evaluateGameState", function () {
        it("exists", function () {
            expect(stateMachine.functionObject.evaluateGameState).toBeDefined();
        });
        it("lets the player know that death has occured", function () {
            spyOn(stateMachine.terminal,"println").and.callThrough();
            stateMachine.functionObject.gameStart(stateMachine);
            stateMachine.player.attributes[CONSTANTS.PLAYER.HEALTH] = 0;
            stateMachine.player.attributes[CONSTANTS.PLAYER.CONSTITUTION] = 0;
            stateMachine.functionObject.evaluateGameState(stateMachine);
            expect(stateMachine.terminal.println).toHaveBeenCalledWith("YOU HAVE DIED");
        });
        it("lets the player know if death is imminent", function () {
            spyOn(stateMachine.terminal,"println").and.callThrough();
            stateMachine.functionObject.gameStart(stateMachine);
            stateMachine.player.attributes[CONSTANTS.PLAYER.HEALTH] = 0;
            stateMachine.player.attributes[CONSTANTS.PLAYER.CONSTITUTION] = 1;
            stateMachine.functionObject.evaluateGameState(stateMachine);
            expect(stateMachine.terminal.println).toHaveBeenCalledWith("YOU ARE DYING");
        });
        it("routes to response option 1 if the player is dead", function () {
            stateMachine.functionObject.gameStart(stateMachine);
            stateMachine.player.attributes[CONSTANTS.PLAYER.HEALTH] = 0;
            stateMachine.player.attributes[CONSTANTS.PLAYER.CONSTITUTION] = 0;
            expect(stateMachine.functionObject.evaluateGameState(stateMachine)).toBe(1);
        });
        it("routes to response option 0 if the player is alive", function () {
            stateMachine.functionObject.gameStart(stateMachine);
            stateMachine.player.attributes[CONSTANTS.PLAYER.HEALTH] = 1;
            stateMachine.player.attributes[CONSTANTS.PLAYER.CONSTITUTION] = 1;
            expect(stateMachine.functionObject.evaluateGameState(stateMachine)).toBe(0);
        });
    });
    describe("processGameState", function () {
        it("exists", function () {
            expect(stateMachine.functionObject.processGameState).toBeDefined();
        });
        describe("monster control", function () {
            beforeEach(function () {
                stateMachine.functionObject.gameStart(stateMachine);
                stateMachine.functionObject.rollNewPlayerStatistics(stateMachine);
                stateMachine.functionObject.spawnMonsters(stateMachine);
            });
            it("it kills a current monster if its health is zero and removes it from current monsters", function () {
                var testMonster = stateMachine.currentMonsters[0];
                testMonster.currentHp = 0;
                stateMachine.functionObject.processGameState(stateMachine);
                expect(stateMachine.currentMonsters[0].name).not.toBe(testMonster.name);
            });
            it("awards gold to the player if a current monster dies", function () {
                stateMachine.player.attributes[CONSTANTS.PLAYER.GOLD] = 0;
                stateMachine.currentMonsters[0].currentHp = 0;
                stateMachine.functionObject.processGameState(stateMachine);
                expect(stateMachine.player.attributes[CONSTANTS.PLAYER.GOLD]).not.toBe(0);
            });
            it("notifies the player of a monster death and any gold reward", function () {
                spyOn(stateMachine.terminal,"println").and.callThrough();
                stateMachine.player.attributes[CONSTANTS.PLAYER.GOLD] = 0;
                var testMonster = stateMachine.currentMonsters[0];
                var x = testMonster.currentGold;
                var y = testMonster.name;
                testMonster.currentHp = 0;
                stateMachine.functionObject.processGameState(stateMachine);
                expect(stateMachine.terminal.println).toHaveBeenCalledWith("THE " + y + " HAS DIED");
                expect(stateMachine.terminal.println).toHaveBeenCalledWith("YOU HAVE GAINED " + x +" GOLD");
            });
            it("if a monster is alive attempt to move it", function () {
                spyOn(stateMachine.functionObject,"moveMonster").and.stub();
                stateMachine.currentMonster = stateMachine.currentMonsters[0];
                stateMachine.functionObject.processGameState(stateMachine);
                expect(stateMachine.functionObject.moveMonster).toHaveBeenCalled();
            });
        });
    });
    describe("moveMonster", function () {
        beforeEach(function () {
            stateMachine.functionObject.gameStart(stateMachine);
        });
        it("exists", function () {
            expect(stateMachine.functionObject.moveMonster).toBeDefined();
        });
        it("randomly 'moves' the current monster if it is alive", function () {
            stateMachine.currentMonster = stateMachine.monsterManual.monsters[0].spawn();
            stateMachine.currentMonster.currentHp = 1;
            spyOn(stateMachine, "rnd").and.callFake(function(x) { return x; });
            spyOn(stateMachine.functionObject, "monsterJump").and.stub();
            stateMachine.functionObject.moveMonster(stateMachine);
            expect(stateMachine.rnd).toHaveBeenCalled();
        });
        it("returns the default route", function () {
            stateMachine.currentMonster = stateMachine.monsterManual.monsters[0].spawn();
            stateMachine.currentMonster.currentHp = 0;
            spyOn(stateMachine, "rnd").and.callFake(function() { return 0; });
            expect(stateMachine.functionObject.moveMonster(stateMachine)).toEqual(0);
        });
    });
    describe("monsterJump", function () {
        beforeEach(function () {
            stateMachine.functionObject.gameStart(stateMachine);
            stateMachine.functionObject.defaultDungeon(stateMachine);
            stateMachine.currentMonster = stateMachine.monsterManual.monsters[0].spawn();
            spyOn(stateMachine,"rnd").and.callFake(function() { return 0; });
        });
        it("exists as a function", function () {
            expect(stateMachine.functionObject.monsterJump).toBeDefined();
            expect(typeof stateMachine.functionObject.monsterJump).toEqual("function");
        });
        it("moves the current monster to a new position", function () {
            spyOn(stateMachine.currentMonster,"teleport").and.stub();
            stateMachine.currentMonster.position = { x: 11, y: 6 };
            stateMachine.functionObject.monsterJump(stateMachine);
            expect(stateMachine.currentMonster.teleport).toHaveBeenCalled();
        });
        it("picks a random new position within a short range at random", function () {
            stateMachine.currentMonster.position = { x: 11, y: 6 };
            stateMachine.functionObject.monsterJump(stateMachine);
            expect(stateMachine.currentMonster.position.x).toBeGreaterThan(6);
            expect(stateMachine.currentMonster.position.y).toBeGreaterThan(2);
            expect(stateMachine.currentMonster.position.x).toBeLessThan(15);
            expect(stateMachine.currentMonster.position.y).toBeLessThan(10);
        });
        it("will not move to within 2 spaces of the current position", function () {
            stateMachine.currentMonster.position = {x: 11, y: 6};
            stateMachine.functionObject.monsterJump(stateMachine);
            var xDelta = Math.abs(11 - stateMachine.currentMonster.position.x);
            var yDelta = Math.abs(6 - stateMachine.currentMonster.position.y);
            expect(xDelta > 2 || yDelta > 2).toBe(true);
        });
    });
    describe("spawnMonsters", function () {
        beforeEach(function () {
            stateMachine.functionObject.gameStart(stateMachine);
            stateMachine.functionObject.defaultDungeon(stateMachine);
        });
        it("exists", function () {
            expect(stateMachine.functionObject.spawnMonsters).toBeDefined();
            expect(typeof stateMachine.functionObject.spawnMonsters).toBe("function");
        });
        it("populates currentMonsters array", function () {
            stateMachine.functionObject.spawnMonsters(stateMachine);
            expect(stateMachine.currentMonsters.length).toBeGreaterThan(0);
        });
        it("sets currentMonster to be the first entry in the array", function () {
            stateMachine.functionObject.spawnMonsters(stateMachine);
            expect(stateMachine.currentMonsters[0]).toBe(stateMachine.currentMonster);
        });
        it("sets the position of each entry to an empty space on the map", function () {
            stateMachine.functionObject.spawnMonsters(stateMachine);
            expect(stateMachine.currentMonsters[0].position.x).not.toEqual(-1);
            expect(stateMachine.currentMonsters[0].position.y).not.toEqual(-1);
        });
    });
});
