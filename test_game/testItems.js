describe("Item", function () {
    it("exists", function () {
        expect(Item).toBeDefined();
    });
    it("creates an item object", function () {
        var test = 1;
        var item = new Item("test", true, false, true, test, 1, 2, 3, "ONE");
        expect(item instanceof Item).toBe(true);
        expect(item.name).toBe("test");
        expect(item.weapon).toBe(true);
        expect(item.armour).toBe(false);
        expect(item.consumable).toBe(true);
        expect(item.consumes).toBe(test);
        expect(item.rangeLimit).toBe(1);
        expect(item.hitFactor).toBe(2);
        expect(item.criticalFactor).toBe(3);
        expect(item.classes).toBe("ONE");
    });
    describe("checkClass", function () {
        var testItem;
        beforeEach(function () {
            testItem = new Item("test", true, false, true, null, 1, 2, 3, "ONE");
        });
        it("exists", function () {
            expect(testItem.checkClass).toBeDefined();
        });
        it("verifies that the given class is allowed to use (own) the item", function () {
            expect(testItem.checkClass("ONE")).toBe(true);
        });
        it("verifies that the given class is not allowed to use the item", function () {
            expect(testItem.checkClass("TWO")).toBe(false);
        });
    });
});

describe("Catalogue", function () {
    it("exists", function () {
        expect(Catalogue).toBeDefined();
    });
    it("create a catalogue of items", function () {
        var catalogue = new Catalogue();
        expect(catalogue instanceof Catalogue).toBe(true);
        for (var i = 0; i < catalogue.items.length; ++i) {
            expect(catalogue.items[i].item instanceof Item).toBe(true);
            expect(catalogue.items[i].price).toBeDefined();
            expect(catalogue.items[i].price).not.toBeNaN();
        }
    });
    describe("getSize", function () {
        it("returns the number of items in the catalogue", function () {
            var catalogue = new Catalogue();
            expect(catalogue.getSize()).toBe(15);
        });
    });
    describe("fetch", function () {
        it("returns the item:price pair at given index if the index is valid", function () {
            var catalogue = new Catalogue();
            var entry = catalogue.fetch(1);
            expect(entry).not.toBeNull();
            expect(entry.item instanceof Item).toBe(true);
            expect(entry.price).not.toBeNaN();
        });
        it("returns null if the index is invalid", function () {
            var catalogue = new Catalogue();
            var entry = catalogue.fetch("ben");
            expect(entry).toBeNull();
        });
    });
    describe("purge", function () {
        it("empties the existing catalogue", function () {
            var catalogue = new Catalogue();
            catalogue.purge();
            expect(catalogue.getSize()).toBe(0);
        });
    });
});

describe("Inventory", function () {
    var inventory;
    var testItem;
    beforeEach(function () {
        inventory = new Inventory();
        testItem = new Item("test",false,false,false,null,1,1,1);
    });
    it("exists", function () {
        expect(inventory).toBeDefined();
    });
    describe("addItem", function () {
        it("adds an item to the inventory with a count of zero", function () {
            inventory.addItem(testItem);
            var test = inventory.items[0];
            expect(test).toBeDefined();
            expect(test.count).toBe(0);
            expect(test.item).toBe(testItem);
        });
    });
    describe("fromCatalogue", function () {
        var catalogue;
        beforeEach(function () {
            catalogue = new Catalogue();
        });
        it("populates an empty inventory from the catalogue of valid items", function () {
            inventory.fromCatalogue(catalogue);
            expect(inventory.getSize()).toBe(catalogue.getSize());
        });
    });
    describe("getSize", function () {
        it("returns the size of the inventory", function () {
            inventory.addItem(testItem);
            expect(inventory.getSize()).toBe(1);
        });
    });
    describe("fetch", function () {
        it("returns the item:count pair from the inventory at the given index", function () {
            inventory.addItem(testItem);
            var testPair = inventory.fetch(0);
            expect(testPair.item).toBe(testItem);
            expect(testPair.count).toBe(0);
        });
    });
    describe("incCount", function () {
        it("increments the count of the given item by 1", function (){
            inventory.addItem(testItem);
            inventory.incCount(0);
            expect(inventory.fetch(0).count).toBe(1);
        });
    });
    describe("decCount", function () {
        it("decrements the count of the given item by 1", function () {
            inventory.addItem(testItem);
            inventory.incCount(0);
            inventory.decCount(0);
            expect(inventory.fetch(0).count).toBe(0);
        });
    });
});