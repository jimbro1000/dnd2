function Terminal(elementId, rows, columns) {
    this.rows = Math.floor(rows);
    this.columns = Math.floor(columns);
    this.elementId = elementId;
    this.element = document.getElementById(this.elementId);
    this.emptyRow = "";
    this.TAB = String.fromCharCode(9);
    this._tabLength = 4;
    if (!this.element) {
        throw('No element with the ID ' + this.elementId + ' was found.');
    }
    if (this.columns < 1) this.columns = 1;
    if (this.rows < 1) this.rows = 1;
    this.row = new Array(this.rows);
    this.grid = new Array(this.rows);
    this._cls = function () {
        this.emptyRow = "";
        for (var i = 0; i < this.columns; ++i) this.emptyRow += " ";
        for (var j = 0; j < this.rows; ++j) this.row[j] = this.emptyRow.slice(0);
    };
    this._draw = function () {
        for (var i = 0; i < this.rows; ++i) {
            var node = this.grid[i];
            node.data = this.row[i];
        }
    };
    this._purgeElement = function () {
        while (this.element.hasChildNodes()) {
            this.element.removeChild(this.element.childNodes[0]);
        }
    };
    this._assembleTerminalElement = function () {
        var preTag = document.createElement("pre");
        this.element.appendChild(preTag);
        for (var i = 0; i < this.rows; ++i) {
            var textNode = document.createTextNode('');
            textNode.data = this.row[i];
            this.grid[i] = textNode;
            preTag.appendChild(textNode);
            if (i < this.rows - 1) {
                preTag.appendChild(document.createElement('br'));
            }
        }
    };
    this._scroll = function () {
        for (var i = 1; i < this.rows; ++i) {
            this.grid[i - 1].data = this.grid[i].data;
        }
        this.grid[this.rows - 1].data = this.emptyRow.slice(0);
    };
    this._purgeElement();
    this._assembleTerminalElement();
    this._cls();
    this._draw();
    this.cursorPosition = {
        row: 0,
        column: 0
    };
    this.savePosition = {
        row: 0,
        column: 0
    };

    this._echo = function (input) {
        this.savePosition =  this._copyCursor(this.cursorPosition);
        this.print(input);
        this.cursorPosition =  this._copyCursor(this.savePosition);
    };

    this._copyCursor = function (originalObject) {
        var copy = {
            row: 0,
            column: 0
        };
        copy.row = parseInt("" + originalObject.row);
        copy.column = parseInt("" + originalObject.column);
        return copy;
    }
}

Terminal.prototype.cls = function () {
    this._cls();
    this._draw();
};

Terminal.prototype._print = function (string) {
    var s = this.grid[this.cursorPosition.row].data;
    var thisLine = "";
    var nextLine = "";
    if (this.cursorPosition.column > 0) thisLine = s.substr(0, this.cursorPosition.column);
    var tabIndex = string.indexOf(this.TAB);
    if (tabIndex !== -1) {
        this._print(string.slice(0,tabIndex));
        var remainder = this._tabLength - (this.cursorPosition.column % this._tabLength);
        if (remainder === 0) remainder = this._tabLength;
        if (this.cursorPosition.column + remainder > this.columns) remainder = this.columns - this.cursorPosition.column;
        var tabSubstitution = "";
        for (var i = 0; i < remainder; i++) tabSubstitution += " ";
        this._print(tabSubstitution + string.slice(tabIndex + 1, string.length));
    } else {
        var endLength = this.cursorPosition.column + string.length;
        if (endLength < this.columns) {
            thisLine += string;
            this.cursorPosition.column = thisLine.length;
            this.grid[this.cursorPosition.row].data = thisLine + s.substring(endLength, this.columns);
        } else {
            nextLine = string.slice(string.length - (endLength - this.columns));
            thisLine += string.substr(0, this.columns - this.cursorPosition.column);
            this.grid[this.cursorPosition.row].data = thisLine;
            ++this.cursorPosition.row;
            if (this.cursorPosition.row === this.rows) {
                this._scroll();
                --this.cursorPosition.row;
                --this.savePosition.row;
            }
            this.cursorPosition.column = 0;
            this._print(nextLine);
        }
    }
};

Terminal.prototype.print = function (string) {
    this._print(string);
};

Terminal.prototype._println = function (string) {
    var s = this.grid[this.cursorPosition.row].data;
    var thisLine = "";
    var nextLine = "";
    if (this.cursorPosition.column > 0) thisLine = s.substr(0, this.cursorPosition.column);
    var tabIndex = string.indexOf(this.TAB);
    if (tabIndex !== -1) {
        this._print(string.slice(0,tabIndex));
        var remainder = this._tabLength - (this.cursorPosition.column % this._tabLength);
        if (remainder === 0) remainder = this._tabLength;
        if (this.cursorPosition.column + remainder > this.columns) remainder = this.columns - this.cursorPosition.column;
        var tabSubstitution = "";
        for (var i = 0; i < remainder; i++) tabSubstitution += " ";
        this._println(tabSubstitution + string.slice(tabIndex + 1, string.length));
    } else {
        var endLength = this.cursorPosition.column + string.length;
        if (endLength < this.columns) {
            thisLine += string;
            this.grid[this.cursorPosition.row].data = thisLine + this.emptyRow.substring(endLength, this.columns);
        } else {
            nextLine = string.slice(string.length - (endLength - this.columns));
            thisLine += string.substr(0, this.columns - this.cursorPosition.column);
            this.grid[this.cursorPosition.row].data = thisLine;
        }
        ++this.cursorPosition.row;
        if (this.cursorPosition.row === this.rows) {
            this._scroll();
            --this.cursorPosition.row;
            --this.savePosition.row;
        }
        this.cursorPosition.column = 0;
        if (nextLine !== "") this._println(nextLine);
    }
};

Terminal.prototype.println = function (string) {
    this._println(string);
};

Terminal.prototype.printc = function (string) {
    var thisLine = string.trim().replace(this.TAB, " ");
    var padding = this.columns - thisLine.length;
    if (padding < 0) {
        var nextLine = thisLine.slice(this.columns);
        this.cursorPosition.column = 0;
        this.println(thisLine.substr(0, this.columns));
        this.printc(nextLine);
    } else {
        if (padding > 0) {
            var leftPad = Math.floor(padding / 2);
            thisLine = this.emptyRow.slice(0, leftPad) + thisLine;
            thisLine += this.emptyRow.slice(0, this.columns - thisLine.length);
        }
        this.grid[this.cursorPosition.row].data = thisLine;
        ++this.cursorPosition.row;
        this.cursorPosition.column = 0;
        if (this.cursorPosition.row === this.rows) {
            this._scroll();
            --this.cursorPosition.row;
            --this.savePosition.row;
        }
    }
};