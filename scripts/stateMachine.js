function StateMachine() {
    this.configurationName = "";
    this.states = 0;
    this.configurationStates = {};
    this.allStates = [];
    this.ready = false;
    this.initialState = undefined;
    this.currentState = undefined;
    this.functionObject = undefined;
    this.inputHandler = new InputHandler();
    this.terminal = undefined;
    this.lastInput = "";

    this._inputCallback = function () {
        return this.currentState === -1 ? 0 : 1;
    };

    this._nulInput = function (callback) {
        setTimeout(callback, 1);
    };

    this._execute = function (state) {
        var result = 0;
        var localMachineState = state.configurationStates[state.currentState];
        var stateFunction = state.functionObject[localMachineState.stateFunction];
        if (typeof(stateFunction) !== "undefined") {
            result = stateFunction(state);
        } else {
            console.error("missing or invalid state function: " + localMachineState.stateFunction);
        }
        return result;
    };
}

StateMachine.prototype.processConfiguration = function (json) {
    if (!json) return false;
    if (json["configurationName"]) {
        this.configurationName = json["configurationName"];
    }
    if (json["stateFunctionObject"]) {
        if (json["stateFunctionObject"] !== "undefined")
            this.functionObject = new window[json["stateFunctionObject"]]();
    } else {
        return false;
    }
    if (json["configurationStates"]) {
        var configStates = json["configurationStates"];
        var parent = this;
        configStates.forEach(function (item, index) {
            var state = new MachineState(item.stateId, item.stateName, item.stateFunction, item.waitForInput, item.results);
            parent.configurationStates[state.id] = state;
            parent.allStates.push(state);
            ++parent.states;
        });
    } else {
        return false;
    }
    if (this.states > 0) {
        if (json["initialState"]) {
            var id = parseInt(json["initialState"]);
            if (this.configurationStates[id]) {
                this.initialState = this.configurationStates[id];
            }
        }
        if (typeof(this.initialState) === "undefined") {
            this.initialState = this.configurationStates[0];
        }
    }
    this.currentState = this.initialState.id;
    return true;
};

StateMachine.prototype.loadConfiguration = function (configurationName) {
    return new Promise(function (resolve, reject) {
        if (typeof(configurationName) !== "string") {
            reject(Error("invalid configuration"));
        }
        this.configurationName = configurationName;
        var xhr = new XMLHttpRequest();
        var loader = this;
        xhr.open("GET", configurationName, true);
        xhr.onreadystatechange = function () {
            if (xhr.readyState === XMLHttpRequest.DONE) {
                if (xhr.status === 200) {
                    var result = JSON.parse(xhr.responseText);
                    resolve(result);
                } else {
                    reject(Error("unable to retrieve configuration"));
                }
            }
        };
        xhr.onerror = function () {
            reject(Error("unable to retrieve configuration"));
        };
        xhr.send();
    });
};

StateMachine.prototype.validateConfigurationStates = function () {
    var result = true;
    var parent = this;
    this.allStates.forEach(function (item) {
        item.results.forEach(function (resultItem) {
            if (resultItem != -1 && typeof(parent.configurationStates[resultItem]) === "undefined") {
                console.warn("Item: " + item.id + " (" + item.name + ") missing reference state " + resultItem);
                result = false;
            }
        });
    });
    return result;
};

StateMachine.prototype.processState = function () {
    var waitAtEnd = false;
    if (this.currentState !== -1) {
        var state = this.configurationStates[this.currentState];
        waitAtEnd = state.waitForInput;
        var outcomeSelector = 0;
        if (typeof(state.stateFunction) !== "undefined") {
            outcomeSelector = this._execute(this);
        }
        this.currentState = state.results[outcomeSelector];
    }
    if (waitAtEnd) {
        this.inputHandler
            .input(this.terminal)
            .then(this._inputCallback());
        return 2;
    } else {
        return this._inputCallback();
    }
};

StateMachine.prototype.executeMachine = function (machine) {
    var waitAtEnd = false;
    if (machine.currentState !== -1) {
        var state = machine.configurationStates[machine.currentState];
        waitAtEnd = state.waitForInput;
        var outcomeSelector = 0;
        if (typeof(state.stateFunction) !== "undefined") {
            outcomeSelector = machine._execute(machine);
        }
        machine.currentState = state.results[outcomeSelector];
    }
    if (waitAtEnd) {
        machine
            .inputHandler
            .input(this.terminal)
            .then(function(inputResult) {
                machine.lastInput = inputResult; machine.executeMachine(machine);
            });
    } else {
        machine
            ._nulInput(function() {
                machine.lastInput = ""; machine.executeMachine(machine);
            });
    }
};
