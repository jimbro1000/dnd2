Item = function (name, weapon, armour, consumable, consumes, rangeLimit, hitFactor, criticalFactor, classes) {
    if (!(this instanceof Item)) {
        return new Item(name, weapon, armour, consumable, consumes, rangeLimit, hitFactor, criticalFactor, classes);
    }
    this.name = name;
    this.weapon = weapon;
    this.armour = armour;
    this.consumable = consumable;
    this.consumes = consumes;
    this.rangeLimit = rangeLimit;
    this.hitFactor = hitFactor;
    this.criticalFactor = criticalFactor;
    this.classes = classes;

    this.checkClass = function( testClass ) {
        return this.classes.indexOf(testClass) !== -1;
    };
};

Catalogue = function () {
    if (!this instanceof Catalogue) {
        return new Catalogue();
    }
    this.items = [];

    this.items.push({item: new Item("SWORD", true, false, false, null, 2, 5 / 11, 6 / 9, "FIGHTER"), price: 10});
    this.items.push({item: new Item("2-H-SWORD", true, false, false, null, 2, 5 / 7, 1, "FIGHTER"), price: 15});
    this.items.push({item: new Item("DAGGER", true, false, false, null, 10, 1 / 4, 4 / 10, "FIGHTER.WIZARD"), price: 3});
    this.items.push({item: new Item("MACE", true, false, false, null, 2, 5 / 11, 5 / 9, "FIGHTER.CLERIC"), price: 5});
    this.items.push({item: new Item("SPEAR", true, false, false, null, 10, 5 / 9, 2 / 3, "FIGHTER"), price: 2});
    var arrow = new Item("ARROW", true, false, true, null, 1.5, 1 / 7, 1 / 5, "FIGHTER");
    this.items.push({item: new Item("BOW", true, false, false, arrow, 15, 3 / 7, 5 / 11, "FIGHTER"), price: 15});
    this.items.push({item: arrow, price: 2});
    this.items.push({item: new Item("LEATHER MAIL", false, true, false, null, 4, 1 / 10, 1 / 8, "FIGHTER.CLERIC.WIZARD"), price: 15});
    this.items.push({item: new Item("CHAIN MAIL", false, true, false, null, 4, 1 / 7, 1 / 6, "FIGHTER.CLERIC"), price: 30});
    this.items.push({item: new Item("PLATE MAIL", false, true, false, null, 3, 1 / 8, 1 / 5, "FIGHTER"), price: 50});
    this.items.push({item: new Item("ROPE", false, false, true, null, 5, 1 / 9, 1 / 6, "FIGHTER.CLERIC.WIZARD"), price: 1});
    this.items.push({item: new Item("SPIKE", false, false, true, null, 8, 1 / 9, 1 / 4, "FIGHTER.CLERIC.WIZARD"), price: 1});
    this.items.push({item: new Item("FLASK OF OIL", false, false, true, null, 6, 1 / 3, 2 / 3, "FIGHTER.CLERIC.WIZARD"), price: 2});
    this.items.push({item: new Item("SILVER CROSS", false, false, true, null, 1.5, 1 / 3, 1 / 2, "FIGHTER.CLERIC.WIZARD"), price: 25});
    this.items.push({item: new Item("SPARE FOOD", false, false, true, null, 10, 1 / 6, 1 / 8, "FIGHTER.CLERIC.WIZARD"), price: 5});

    this.getSize = function () {
        return this.items.length;
    };

    this.fetch = function (index) {
        if (!isNaN(index) && index >= 0 && index < this.items.length) {
            return this.items[index];
        } else {
            return null;
        }
    };

    this.purge = function () {
        this.items = [];
    };
};

Inventory = function () {
    if (!this instanceof Inventory) {
        return new Inventory();
    }
    this.items = [];

    this.addItem = function (item) {
        this.items.push({item: item, count: 0});
    };

    this.getSize = function () {
        return this.items.length;
    };

    this.getCount = function () {
        var count = 0;
        for (var i = 0; i < this.items.length; ++i) {
            count += this.items[i].count;
        }
        return count;
    };

    this.fromCatalogue = function (catalogue) {
        for (var i = 0; i < catalogue.getSize(); ++i) {
            this.addItem(catalogue.fetch(i).item);
        }
    };

    this.fetch = function (index) {
        return this.items[index];
    };

    this.incCount = function (index) {
        this.items[index].count++;
    };

    this.decCount = function (index) {
        this.items[index].count = Math.max(0, this.items[index].count - 1);
    };
};