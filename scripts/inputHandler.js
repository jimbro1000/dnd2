function InputHandler() {
    this.BUFFER_LENGTH = 255;
    this.inputBuffer = new Array(this.BUFFER_LENGTH);
    this.inputPointer = 0;
    this.nextPointer = 0;
    this.reading = false;
    this.container = window;
    this.waitForInput = false;
    this._inputCallback = undefined;
    this._echoTerminal = undefined;
    this._inputReject = undefined;
    this.INPUT_TO_UPPER = 1;
    this.INPUT_TO_LOWER = 2;
    this.INPUT_NATURAL = 0;
    this.inputConvert = this.INPUT_NATURAL;

    this._convert = function(input) {
        var result = input;
        if (this.inputConvert === this.INPUT_TO_UPPER) {
            result = result.toUpperCase();
        } else if (this.inputConvert === this.INPUT_TO_LOWER) {
            result = result.toLowerCase();
        }
        return result;
    };

    this._toString = function () {
        var index = this.inputPointer;
        var result = "";
        while (index != this.nextPointer) {
            result += String.fromCharCode(this.inputBuffer[index]);
            index = ++index % this.BUFFER_LENGTH;
        }
        return result;
    };

    this._sniffInputString = function () {
        var result = "";
        var sniffPointer = this.inputPointer;
        while (sniffPointer != this.nextPointer && this.inputBuffer[sniffPointer] !== 13) {
            nextChar = this.inputBuffer[sniffPointer];
            if (nextChar === 8) {
                if (result !== "") {
                    result = result.substr(0,result.length - 1);
                }
            } else {
                result += String.fromCharCode(nextChar);
            }
            sniffPointer = ++sniffPointer % this.BUFFER_LENGTH;
        }
        return this._convert(result) + " ";
    };

    this._inputString = function () {
        var result = "";
        while (this.inputPointer != this.nextPointer && this.inputBuffer[this.inputPointer] !== 13) {
            nextChar = this.inputBuffer[this.inputPointer];
            if (nextChar === 8) {
                if (result !== "") {
                    result = result.substr(0,result.length - 1);
                }
            } else {
                result += String.fromCharCode(nextChar);
            }
            this.inputPointer = ++this.inputPointer % this.BUFFER_LENGTH;
        }
        if (this.inputBuffer[this.inputPointer] === 13) {
            this.inputPointer = ++this.inputPointer % this.BUFFER_LENGTH;
        }
        return this._convert(result);
    };

    this._keyup = function (event) {
        if (this.reading && event.keyCode === 8) {
            this.addNext(event.keyCode);
        }
    };

    this._keypress = function (event) {
        if (this.reading) {
            this.addNext(event.keyCode);
        }
    };

    safe_event.addEventListener(this.container, "keypress", this._keypress, this);
    safe_event.addEventListener(this.container, "keyup", this._keyup, this);
}

InputHandler.prototype.removeEventHooks = function () {
    safe_event.removeEventListener(this.container, "keypress", this._keypress, this);
    safe_event.removeEventListener(this.container, "keyup", this._keyup, this);
};

InputHandler.prototype.getNext = function () {
    var result = null;
    if (this.nextPointer != this.inputPointer) {
        result = this.inputBuffer[this.inputPointer++];
        this.inputPointer = this.inputPointer % this.BUFFER_LENGTH;
    }
    return result;
};

InputHandler.prototype.addNext = function (keyCode) {
    if (!((this.nextPointer + 1) % this.BUFFER_LENGTH === this.inputBuffer)) {
        this.inputBuffer[this.nextPointer++] = keyCode;
        if (this.waitForInput) {
            if (keyCode == 13) {
                var result = this._inputString();
                if (this._echoTerminal) {
                    this._echoTerminal.println(result);
                    this._echoTerminal = undefined;
                }
                var callback = this._inputCallback;
                this._inputCallback = undefined;
                this._inputReject = undefined;
                this.waitForInput = false;
                callback(result);
            } else {
                if (this._echoTerminal) {
                    this._echoTerminal._echo(this._sniffInputString());
                }
            }
        }
    }
};

InputHandler.prototype.startReading = function () {
    this.reading = true;
};

InputHandler.prototype.stopReading = function () {
    this.reading = false;
};

InputHandler.prototype.input = function (terminal) {
    if (terminal) {
        this._echoTerminal = terminal;
    }
    this.waitForInput = true;
    this.startReading();
    var promiseCallback;
    var promiseReject;
    var promise = new Promise(function(resolve, reject) {
        promiseCallback = resolve;
        promiseReject = reject;
    });
    this._inputCallback = promiseCallback;
    this._inputReject = promiseReject;
    return promise;
};

safe_event = {
    "addEventListener": function (element, eventName, eventHandler, scope) {
        var scopedEventHandler = scope ? function (e) {
            eventHandler.apply(scope, [e]);
        } : eventHandler;
        if (element.addEventListener) {
            element.addEventListener(eventName, scopedEventHandler, false);
        } else if (document.attachEvent) {
            element.attachEvent("on" + eventName, scopedEventHandler);
        }
    },
    "removeEventListener": function (element, eventName, eventHandler, scope) {
        var scopedEventHandler = scope ? function (e) {
            eventHandler.apply(scope, [e]);
        } : eventHandler;
        if (element.removeEventListener)
            element.removeEventListener(eventName, scopedEventHandler, false);
        else if (element.detachEvent)
            element.detachEvent("on" + eventName, scopedEventHandler);
    }
};
