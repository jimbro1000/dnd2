Monster = function(nameOrEncoded, level, strength, hp, experience, gold) {
    if (level === undefined) {
        var attributes = nameOrEncoded.split("|");
        this.name = attributes[0];
        for (var i=1; i<attributes.length; ++i) {
            attributes[i] = parseInt(attributes[i]);
        }
        this.level = attributes[1];
        this.strength = attributes[2];
        this.hp = attributes[3];
        this.experience = attributes[4];
        this.gold = attributes[5];
    } else {
        this.name = nameOrEncoded;
        this.level = level;
        this.strength = strength;
        this.hp = hp;
        this.experience = experience;
        this.gold = gold;
    }
    this.currentHp = 0;
    this.currentGold = 0;
    this.position = { x: 0, y: 0 };

    this.spawn = function() {
        var child = new Monster(this.name, this.level, this.strength, this.hp, this.experience, this.gold);
        child.currentHp = child.hp;
        child.currentGold = child.gold;
        child.stringEncode = function () {
            return this.name + "|" + this.currentHp + "|" + this.currentGold;
        };
        child.position = {x: -1, y: -1};
        return child;
    };

    this.stringEncode = function () {
        return this.name + "|" + this.level + "|" + this.strength + "|" + this.hp + "|" + this.experience + "|" + this.gold;
    };

    this.teleport = function(newXPosition, newYPosition, dungeon) {
        if (this.position.x !== -1 && this.position.y !== -1) {
            dungeon.setAt(this.position.x, this.position.y, this.standingOn);
        }
        this.position = { x: newXPosition, y: newYPosition };
        this.standingOn = dungeon.lookAt(this.position.x, this.position.y);
    };
};

MonsterManual = function() {
    this.monsters = [];

    this.add = function(monster) {
        if (!this.contains(monster.name)) {
            this.monsters.push(monster);
        }
    };

    this.fetch = function (name) {
        var result = null;
        for(var i=0; i<this.monsters.length; ++i) {
            if (this.monsters[i].name === name) {
                result = this.monsters[i];
                break;
            }
        }
        return result;
    };

    this.contains = function(name) {
        return this.fetch(name) !== null;
    };

    this.stringEncode = function(index) {
        return this.monsters[index].stringEncode();
    };

    this.stringDecode = function(encodedMonster) {
        this.add(new Monster(encodedMonster));
    };

    this.purge = function() {
        this.monsters = [];
    };

};
