Dungeon = function (size) {
    this.EMPTY = 0;
    this.size = size;
    this.grid = Array.apply(null, Array(this.size)).map(function () {
    });
    for (var i = 0; i < this.size; ++i) {
        this.grid[i] = Array.apply(null, Array(this.size)).map(function () {
            return 0;
        });
    }

    this.lookAt = function (x, y) {
        return this.grid[x][y];
    };

    this.setAt = function (x, y, n) {
        this.grid[x][y] = n;
    };

    this.fromString = function (row, valueString) {
        if (row >= 0 && row < this.size && valueString.length >= this.size) {
            for (var i = 0; i < this.size; ++i) {
                var c = valueString.slice(0, 1);
                valueString = valueString.slice(1);
                this.grid[row][i] = parseInt(c);
            }
        }
    };

    this.toString = function (row) {
        var result = "";
        if (row >= 0 && row < this.size) {
            for (var i = 0; i < this.size; ++i) {
                result += "" + this.grid[row][i];
            }
        }
        return result;
    };
};

