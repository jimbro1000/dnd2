CONSTANTS = {
    PLAYER : {
        CLASS : 0,
        STRENGTH : 1,
        DEXTERITY : 2,
        CONSTITUTION : 3,
        CHARISMA : 4,
        WISDOM : 5,
        INTELLIGENCE : 6,
        GOLD : 7,
        HEALTH : 8,
        FIGHTER_HEALTH : 8,
        CLERIC_HEALTH : 6,
        WIZARD_HEALTH : 4,
        FIRST_STAT_ROLL : 1,
        LAST_STAT_ROLL : 7,
        LAST_STAT : 8
    },
    GOLD_MULTIPLIER : 15,
    STAT_ROLL_DICE : 6,
    STAT_DICE_ROLLS : 3,
    TTL_DAYS : 365,
    TTL_MULTIPLIER : (24 * 60 * 60 * 1000)
};

Game = function() {
    if (!(this instanceof Game)) {
        return new Game();
    }
    this.gameExit = function (state) {
        state.terminal.println("STOP");
        return 0;
    };
    this.gameStart = function (state) {
        state.player = {};
        state.player.attributeNames = ["CLASS", "STR", "DEX", "CON", "CHAR", "WIS", "INT", "GOLD", "HP"];
        state.player.attributes = [];
        state.player.position = {x: 0, y: 0};
        state.catalogue = new Catalogue();
        state.monsterManual = new MonsterManual();
        state.functionObject.populateManual(state);
        state.player.inventory = new Inventory();
        state.player.inventory.fromCatalogue(state.catalogue);
        state.player.weapon = -1;
        state.dungeon = new Dungeon(1);
        state.weaponChoice = 0;
        state.currentMonsters = [];

        state.arrayRemoveAt = function (array, x) {
            if (array.length > 0 && x < array.length) {
                array.splice(x,1);
            }
        };

        state.rnd = function (x) {
            return (isNaN(x) || x === 0) ? Math.random() : x * Math.random();
        };
        state.int = function (x) {
            return (isNaN(x)) ? 0 : (x < 0) ? Math.ceil(x) : Math.floor(x);
        };

        state.writeCookie = function (key, value) {
            if (value === undefined) value = "";
            var d = new Date();
            d.setTime(d.getTime() + (CONSTANTS.TTL_DAYS * CONSTANTS.TTL_MULTIPLIER));
            document.cookie = key + "=" + value + ";expires=" + d.toUTCString();
        };

        state.readCookie = function (key) {
            var result = null;
            if (key !== undefined) {
                key += "=";
                var cookieParts = document.cookie.split(";");
                for (var index=0; index<cookieParts.length; ++index) {
                    var part = cookieParts[index].trim();
                    if (part.indexOf(key) === 0) {
                        result = part.slice(key.length);
                        break;
                    }
                }
            }
            return result;
        };

        return 0;
    };
    this.showWelcomeMessage = function (state) {
        state.terminal.cls();
        state.terminal.printc("DUNGEONS AND DRAGONS");
        state.terminal.printc("(C) 1977 RICHARD GARRIOTT");
        state.terminal.printc("PORTED BY JULIAN BROWN");
        state.terminal.printc("ALL RIGHTS TO THIS PORT REMAIN PROPERTY");
        state.terminal.printc("OF RICHARD GARRIOTT");
        state.terminal.printc("****** UPDATED DECEMBER 2016 ******");
        state.terminal.printc("");
        return 0;
    };
    this.askIfInstructionsAreNeeded = function (state) {
        state.terminal.println("DO YOU NEED INSTRUCTIONS?");
        return 0;
    };
    this.handleRequestForInstructions = function (state) {
        var input = state.lastInput.toUpperCase();
        if (input === "Y") {
            state.terminal.println("WHO SAID YOU COULD PLAY!");
            return 0;
        } else if (input === "N") {
            return 1;
        } else {
            state.terminal.println("WHAT?");
            return 2;
        }
    };
    this.promptOldOrNewGame = function (state) {
        state.terminal.println("OLD OR NEW GAME?");
        return 0;
    };
    this.handleOldOrNewGame = function (state) {
        if (state.lastInput === "OLD") {
            return 0;
        } else if (state.lastInput === "NEW") {
            return 1;
        } else {
            state.terminal.println("WHAT?");
            return 2;
        }
    };
    this.promptDungeonNumber = function (state) {
        state.terminal.println("DUNGEON NUMBER#");
        return 0;
    };
    this.acceptDungeonNumber = function (state) {
        state.dungeonNumber = parseInt(state.lastInput);
        var result = 1;
        if (!isNaN(state.dungeonNumber)) {
            if (state.dungeonNumber > 0 && state.dungeonNumber < 7) {
                result = 0;
            }
        }
        return result;
    };
    this.loadSavedGame = function (state) {
        var player = state.readCookie("player").split("|");
        for(var attributeIndex = 0; attributeIndex < player.length; ++attributeIndex) {
            var item = player[attributeIndex];
            if (attributeIndex === 0) {
                state.player.attributes[attributeIndex] = item;
            } else {
                state.player.attributes[attributeIndex] = parseInt(item);
            }
        }
        var itemCount = parseInt(state.readCookie("game.catalogue"));
        state.catalogue.purge();
        for (var itemIndex = 0; itemIndex < itemCount; ++itemIndex) {
            var ip = state.readCookie("game.catalogue." + itemIndex).split("|");
            item = new Item(ip[1],ip[2]==='true',ip[3]==='true',ip[4]==='true',ip[5]==='null'?null:ip[5],parseInt(ip[6]),parseFloat(ip[7]),parseFloat(ip[8]),ip[9]);
            state.catalogue.items.push({item: item, price: parseInt(ip[0])});
        }
        for(itemIndex = 0; itemIndex < state.catalogue.items.length; ++itemIndex) {
            item = state.catalogue.items[itemIndex];
            if (item.item[4] !== null) {
                var searchingFor = item.item[4];
                var found = null;
                for (var index = 9; index < state.catalogue.items.length; ++index) {
                    var otherItem = state.catalogue.items[index];
                    if (otherItem.item[0] === searchingFor) {
                        found = otherItem.item;
                        break;
                    }
                }
                item.item[4] = found;
            }
        }
        state.player.inventory.items = [];
        state.player.inventory.fromCatalogue(state.catalogue);
        for (itemIndex = 0; itemIndex < state.player.inventory.items.length; ++itemIndex) {
            state.player.inventory.items[itemIndex].coount = parseInt(state.readCookie("player.inventory." + itemIndex));
        }
        state.dungeon = new Dungeon(parseInt(state.readCookie("game.dungeon")));
        for (var columnIndex=0; columnIndex < state.dungeon.size; ++columnIndex) {
            state.dungeon.fromString(columnIndex, state.readCookie("game.dungeon." + columnIndex));
        }
        state.monsterManual.purge();
        var monsterCount = state.readCookie("game.monster");
        for (var monsterIndex = 0; monsterIndex < monsterCount; ++monsterIndex) {
            state.monsterManual.add(new Monster(state.readCookie("game.monster." + monsterIndex)));
        }
        monsterCount = state.readCookie("game.current.monsters");
        state.currentMonsters = [];
        for (monsterIndex = 0; monsterIndex < monsterCount; ++monsterIndex) {
            state.currentMonsters.push(new Monster(state.readCookie("game.current.monster." + monsterIndex)));
        }
        state.currentState = parseInt(state.readCookie("state"));
        return 0;
    };
    this.loadDungeonNumber = function (state) {
        if (state.dungeonNumber === 1) {
            state.functionObject.defaultDungeon(state);
            state.functionObject.spawnMonsters(state);
        }
        return 0;
    };
    this.defaultDungeon = function (state) {
        state.dungeon = new Dungeon(26);
        state.dungeon.fromString(0, "11111111111111111111111111");
        state.dungeon.fromString(1, "10000000400030161000100001");
        state.dungeon.fromString(2, "10111110111010001210101101");
        state.dungeon.fromString(3, "10001020100011111030100101");
        state.dungeon.fromString(4, "11131011101010040010110101");
        state.dungeon.fromString(5, "10061006101000111116100101");
        state.dungeon.fromString(6, "10111011101010100011110101");
        state.dungeon.fromString(7, "10000000111010001016011101");
        state.dungeon.fromString(8, "11113111100014100010010001");
        state.dungeon.fromString(9, "10610000101110111111010111");
        state.dungeon.fromString(10, "10110111101000010003010101");
        state.dungeon.fromString(11, "10000000001000010001110101");
        state.dungeon.fromString(12, "11110114111600040001000101");
        state.dungeon.fromString(13, "10000010001111111111110001");
        state.dungeon.fromString(14, "10111111100000100010000111");
        state.dungeon.fromString(15, "10101010111010101010110001");
        state.dungeon.fromString(16, "10101000000010111010011101");
        state.dungeon.fromString(17, "10101011101000000011010101");
        state.dungeon.fromString(18, "10100010131010111010010001");
        state.dungeon.fromString(19, "10101110301110001000111111");
        state.dungeon.fromString(20, "10001000100011101010100001");
        state.dungeon.fromString(21, "11111311111010001010101101");
        state.dungeon.fromString(22, "16001001001040111010101001");
        state.dungeon.fromString(23, "11100024011010001010001011");
        state.dungeon.fromString(24, "16001001000011100010101001");
        state.dungeon.fromString(25, "11111111111111111111111111");
        state.player.position.x = 1;
        state.player.position.y = 1;
    };
    this.askPlayerName = function (state) {
        state.terminal.println("PLAYER NAME");
        return 0;
    };
    this.acceptPlayerName = function (state) {
        if (state.lastInput === "SHAVS") return 0;
        else {
            state.terminal.println("WHO SAID YOU COULD PLAY!");
            return 1;
        }
    };
    this.rollNewPlayerStatistics = function (state) {
        for (var M = CONSTANTS.PLAYER.FIRST_STAT_ROLL; M <= CONSTANTS.PLAYER.LAST_STAT_ROLL; M++) {
            state.player.attributes[M] = 0;
            for (var N = 0; N < CONSTANTS.STAT_DICE_ROLLS; N++) {
                state.player.attributes[M] += state.int(state.rnd(CONSTANTS.STAT_ROLL_DICE) + 1);
            }
        }
        state.player.attributes[CONSTANTS.PLAYER.GOLD] *= CONSTANTS.GOLD_MULTIPLIER;
        state.player.attributes[CONSTANTS.PLAYER.CLASS] = "";
        state.player.attributes[CONSTANTS.PLAYER.HEALTH] = 0;
        return 0;
    };
    this.promptPlayerClass = function (state) {
        state.terminal.println("CLASSIFICATION");
        state.terminal.println("WHICH DO YOU WANT TO BE");
        state.terminal.println("FIGHTER, CLERIC OR WIZARD");
        return 0;
    };
    this.acceptPlayerClass = function (state) {
        var input = state.lastInput.trim();
        var result = 1;
        if (input === "FIGHTER") {
            state.player.attributes[CONSTANTS.PLAYER.CLASS] = input;
            state.player.attributes[CONSTANTS.PLAYER.HEALTH] = state.int(state.rnd(CONSTANTS.PLAYER.FIGHTER_HEALTH) + 1);
        } else if (input === "CLERIC") {
            state.player.attributes[CONSTANTS.PLAYER.CLASS] = input;
            state.player.attributes[CONSTANTS.PLAYER.HEALTH] = state.int(state.rnd(CONSTANTS.PLAYER.CLERIC_HEALTH) + 1);
        } else if (input === "WIZARD") {
            state.player.attributes[CONSTANTS.PLAYER.CLASS] = input;
            state.player.attributes[CONSTANTS.PLAYER.HEALTH] = state.int(state.rnd(CONSTANTS.PLAYER.WIZARD_HEALTH) + 1);
        } else if (input === "REROLL") {
            result = 2;
        } else {
            state.terminal.println("WHAT?");
            result = 0;
        }
        return result;
    };
    this.promptUserForShopInventory = function (state) {
        state.terminal.println("BUYING WEAPONS");
        state.terminal.println("FAST OR NORMAL");
        return 0;
    };
    this.acceptShopInventory = function (state) {
        var input = state.lastInput.trim();
        var result = 2;
        if (input === "FAST") {
            result = 0;
        } else if (input === "NORMAL") {
            result = 1;
        } else {
            state.terminal.println("WHAT?");
        }
        return result;
    };
    this.listShopInventory = function (state) {
        state.terminal.println("NUMBER" + state.terminal.TAB + "ITEM" + state.terminal.TAB + "PRICE");
        for(var i = 0; i < state.catalogue.getSize(); ++i) {
            var item = state.catalogue.fetch(i);
            state.terminal.println((i + 1) + state.terminal.TAB + item.item.name + state.terminal.TAB + item.price);
        }
        state.terminal.println("-1 - STOP; -2 - LIST ITEMS");
        return 0;
    };
    this.promptUserForItem = function (state) {
        state.terminal.print(">");
        return 0;
    };
    this.acceptShopItem = function (state) {
        var input = parseInt(state.lastInput);
        var result = 0;
        if (isNaN(input)) {
            state.terminal.println("YOU MUST INPUT A NUMBER");
        } else {
            if (input > 0) {
                if (input > state.catalogue.getSize()) {
                    state.terminal.println("THAT ITEM DOESN'T EXIST");
                } else {
                    var item = state.catalogue.fetch(input - 1);
                    if (!item.item.checkClass(state.player.attributes[CONSTANTS.PLAYER.CLASS])) {
                        state.terminal.println("YOU CAN'T USE THAT");
                    } else if (state.player.attributes[CONSTANTS.PLAYER.GOLD] < item.price) {
                        state.terminal.println("COSTS TOO MUCH");
                    } else {
                        state.player.attributes[CONSTANTS.PLAYER.GOLD] -= item.price;
                        state.player.inventory.incCount(input - 1);
                    }
                }
            } else {
                if (input === -1) result = 2;
                else if (input === -2) result = 1;
                else state.terminal.println("WHAT?");
            }
        }
        return result;
    };
    this.promptShowInventory = function(state) {
        state.terminal.println("DO YOU WANT TO SEE YOUR INVENTORY?");
        return 0;
    };
    this.acceptShowInventory = function(state) {
        var result = 2;
        var input = state.lastInput.trim();
        if (input === "Y") {
            result = 0;
        } else if (input === "N") {
            result = 1;
        } else {
            state.terminal.println("WHAT?");
        }
        return result;
    };
    this.showInventory = function(state) {
        var inventory = state.player.inventory;
        if (inventory.getCount() === 0) {
            state.terminal.println("YOU AREN'T CARRYING ANYTHING");
        } else {
            state.terminal.println("YOU ARE CARRYING:");
            for (var i = 0; i < state.player.inventory.getSize(); ++i) {
                var item = inventory.fetch(i);
                if (item.count > 0) {
                    state.terminal.println(item.count + " OF " + item.item.name);
                }
            }
        }
        return 0;
    };
    this.showStats = function(state) {
        for (var i=0; i<9; ++i) {
            if (state.player.attributes[i] && state.player.attributes[i] !== "" && state.player.attributes[i] !== 0) {
                state.terminal.println(state.player.attributeNames[i] + ": " + state.player.attributes[i]);
            }
        }
        return 0;
    };
    this.gameLoop = function(state) {
        state.terminal.print(">");
        return 0;
    };
    this.acceptPlayerInput = function(state) {
        var input = state.lastInput.trim();
        var result = 0;
        if (input === "0") {
        } else if (input === "1") {
            result = 4;
        } else if (input === "99") {
            result = 1;
        } else if (input === "12") {
            result = 3;
        } else if (input === "3") {
            result = 5;
        } else if (input === "4") {
            result = 6;
        } else if (input === "5") {
            result = 7;
        } else if (input === "2") {
            result = 8;
        } else if (input === "6") {
            result = 9;
        } else if (input === "7") {
            result = 10;
        } else if (input === "8") {
            result = 11;
        } else {
            result = 2;
            state.terminal.println("WHAT?");
        }
        return result;
    };
    this.quitGame = function(state) {
        state.terminal.println("GOODBYE");
        return 0;
    };
    this.showCommands = function(state) {
        var t = state.terminal.TAB;
        state.terminal.println("AVAILABLE COMMANDS:");
        state.terminal.println("1=MOVE" + t + "2=OPEN DOOR" + t + "3=SEARCH FOR TRAPS AND SECRET DOORS");
        state.terminal.println("4=SWITCH WEAPON IN HAND" + t + "5=FIGHT");
        state.terminal.println("6=LOOK AROUND" + t + "7=SAVE GAME" + t + "8=USE MAGIC" + t + "9=BUY MAGIC");
        state.terminal.println("0=PASS" + t + "11=BUY H.P." + t + "12=SHOW COMMANDS");
        return 0;
    };
    this.processGameState = function(state) {
         for (var index = 0; index < state.currentMonsters.length; ++index) {
            currentMonster = state.currentMonsters[index];
            if (currentMonster.currentHp <= 0) {
                state.terminal.println("THE " + currentMonster.name + " HAS DIED");
                state.terminal.println("YOU HAVE GAINED " + currentMonster.gold + " GOLD");
                state.player.attributes[CONSTANTS.PLAYER.GOLD] += currentMonster.gold;
                state.arrayRemoveAt(state.currentMonsters, index);
            } else {
                state.functionObject.moveMonster(state);
            }
        }
        if (state.currentMonsters.length === 0) {
        //level win
        }
        return 0;
    };
    this.evaluateGameState = function(state) {
        var result = 0;
        if (state.player.attributes[CONSTANTS.PLAYER.HEALTH] === 0) {
            if (state.player.attributes[CONSTANTS.PLAYER.CONSTITUTION] > 0) {
                state.terminal.println("YOU ARE DYING");
            } else {
                state.terminal.println("YOU HAVE DIED");
                result = 1;
            }
        }
        return result;
    };
    // this.mockCycle = function(state) {
    //     return 0;
    // };
    this.promptPlayerMoveDirection = function (state) {
        state.terminal.println("YOU ARE AT X:" + state.player.position.x + " Y:" + state.player.position.y);
        state.terminal.println("DIRECTION (1 = NORTH, 2 = EAST, 3 = SOUTH, 4 = WEST)");
        state.terminal.print(">");
        return 0;
    };
    this.acceptPlayerMoveDirection = function (state) {
        var input = state.lastInput.trim();
        var result = 0;
        if (input === "1" || input === "2" || input === "3" || input === "4") {
            state.player.move = parseInt(input);
            result = 1;
        } else {
            state.terminal.println("WHAT?");
            state.player.move = undefined;
        }
        return result;
    };
    this.processPlayerMove = function (state) {
        var direction = state.player.move;
        var dx, dy;
        if (direction === 1) {
            dx = 0;
            dy = -1;
        }
        else if (direction === 2) {
            dx = 1;
            dy = 0;
        }
        else if (direction === 3) {
            dx = 0;
            dy = 1;
        }
        else if (direction === 4) {
            dx = -1;
            dy = 0;
        }
        var lookAtBlock = state.dungeon.lookAt(state.player.position.x + dx, state.player.position.y + dy);
        if (lookAtBlock === 0) {
            state.player.move = 0;
            state.player.position.x += dx;
            state.player.position.y += dy;
        } else {
            state.terminal.println("THE WAY IS BLOCKED");
        }
        return 0;
    };
    this.processPlayerSearch = function (state) {
        state.terminal.println("SEARCHING...");
        var finds = 0;
        if (state.rnd(40) > state.player.attributes[CONSTANTS.PLAYER.WISDOM] + state.player.attributes[CONSTANTS.PLAYER.INTELLIGENCE]) {
            for (var i = -1; i < 2; ++i) for (var j = -1; j < 2; ++j) {
                if (!(i === 0 && j === 0)) {
                    var x = state.player.position.x + i;
                    var y = state.player.position.y + j;
                    var cell = state.dungeon.lookAt(x, y);
                    if (cell === 3) {
                        state.terminal.println("FOUND A DOOR AT X:" + x + " Y:" + y);
                        ++finds;
                    } else if (cell === 2) {
                        state.terminal.println("FOUND A TRAP AT X:" + x + " Y:" + y);
                        ++finds;
                    }
                }
            }
        }
        if (finds === 0) {
            state.terminal.println("DIDN'T FIND ANYTHING");
        }
        return 0;
    };
    this.promptPlayerForWeapon = function (state) {
        state.terminal.println("WHICH WEAPON DO YOU WANT TO USE?");
        state.terminal.print(">");
        return 0;
    };
    this.acceptPlayerWeaponChoice = function (state) {
        var result = 0;
        var input = parseInt(state.lastInput);
        if (isNaN(input) || input < 1 || input > state.catalogue.getSize() + 1) {
            state.terminal.println("WHAT?");
        } else {
            --input;
            if (state.player.inventory.fetch(input).count === 0) {
                state.terminal.println("YOU AREN'T CARRYING ANY");
            } else {
                state.weaponChoice = input + 1;
                result = 1;
            }
        }
        return result;
    };
    this.processPlayerWeaponChoice = function (state) {
        state.player.weapon = state.weaponChoice - 1;
        state.terminal.println("OK YOU ARE NOW HOLDING A " + state.player.inventory.fetch(state.player.weapon).item.name);
        return 0;
    };
    this.processPlayerLook = function (state) {
        for (var yDelta = -5; yDelta < 6; ++yDelta) {
            var viewLine = "";
            for (var xDelta = -5; xDelta < 6; ++xDelta) {
                var x = (state.player.position.x + xDelta + state.dungeon.size) % state.dungeon.size;
                var y = (state.player.position.y + yDelta + state.dungeon.size) % state.dungeon.size;
                if (y >= 0 && x >= 0 && y < state.dungeon.size && x < state.dungeon.size) {
                    if (xDelta === 0 && yDelta === 0) {
                        viewLine += "@";
                    } else {
                        var cellContent = state.dungeon.lookAt(x, y);
                        if (cellContent === 3) cellContent = 1;
                        else if (cellContent === 2) cellContent = 0;
                        viewLine += cellContent;
                    }
                }
            }
            if (viewLine !== "") {
                state.terminal.println(viewLine);
            }
        }
        return 0;
    };
    this.processSaveGame = function (state) {
        var player = state.player.attributes[0];
        for (var index=1; index<=CONSTANTS.PLAYER.LAST_STAT; ++index) {
            player += "|" + state.player.attributes[index];
        }
        state.writeCookie("player",player);
        var inventory = state.player.inventory.fetch(0).count;
        for (index=1; index<state.player.inventory.getSize(); ++index) {
            inventory += "|" + state.player.inventory.fetch(index).count;
        }
        state.writeCookie("player.inventory",inventory);
        state.writeCookie("game.catalogue", "" + state.catalogue.getSize());
        for (index=0; index<state.catalogue.getSize(); ++index) {
            var item = state.catalogue.fetch(index);
            var catalogue = "" + item.price;
            catalogue += "|" + item.item.name;
            catalogue += "|" + item.item.weapon;
            catalogue += "|" + item.item.armour;
            catalogue += "|" + item.item.consumable;
            catalogue += "|" + item.item.consumes;
            catalogue += "|" + item.item.rangeLimit;
            catalogue += "|" + item.item.hitFactor;
            catalogue += "|" + item.item.criticalFactor;
            catalogue += "|" + item.item.classes;
            state.writeCookie("game.catalogue." + index, catalogue);
        }
        state.writeCookie("game.dungeon", "" + state.dungeon.size);
        for (index=0; index<state.dungeon.size; ++index) {
            var dungeon = state.dungeon.toString(index);
            state.writeCookie("game.dungeon." + index, dungeon);
        }
        state.writeCookie("game.monster", "" + state.monsterManual.monsters.length);
        for (index=0; index < state.monsterManual.monsters.length; ++index) {
            state.writeCookie("game.monster." + index, state.monsterManual.stringEncode(index));
        }
        state.writeCookie("game.current.monsters", "" + state.currentMonsters.length);
        for (index=0; index < state.currentMonsters.length; ++index) {
            state.writeCookie("game.current.monster." + index, state.currentMonsters[index].stringEncode());
        }
        state.writeCookie("state","" + state.currentState);
        return 0;
    };
    this.promptUserForReset = function (state) {
        state.terminal.println("RESET?");
        state.terminal.print(">");
        return 0;
    };
    this.acceptReset = function (state) {
        var result = 0;
        if (state.lastInput === "Y") {
            //perform reset (TODO)
        } else if (state.lastInput !== "N") {
            result = 1;
        }
        return result;
    };
    this.populateManual = function (state) {
        if (state.monsterManual !== undefined) {
            state.monsterManual.add(new Monster("MAN", 1, 13, 26, 1, 500));
            state.monsterManual.add(new Monster("GOBLIN", 2, 13, 24, 1, 600));
            state.monsterManual.add(new Monster("TROLL", 3, 15, 35, 1, 1, 1000));
            state.monsterManual.add(new Monster("SKELETON", 4, 22, 12, 1, 1, 50));
            state.monsterManual.add(new Monster("BALROG", 5, 18, 110, 1, 1, 5000));
            state.monsterManual.add(new Monster("OCHRE JELLY", 6, 11, 20, 1, 1, 0));
            state.monsterManual.add(new Monster("GREY OOZE", 7, 11, 13, 1, 1, 0));
            state.monsterManual.add(new Monster("GNOME", 8, 13, 30, 1, 1, 100));
            state.monsterManual.add(new Monster("KOBOLD", 9, 15, 16, 1, 1, 500));
            state.monsterManual.add(new Monster("MUMMY", 10, 16, 30, 1, 1, 100));
        }
    };
    this.moveMonster = function (state) {
        if (state.currentMonster.currentHp > 0) {
            if (state.rnd(1) > 0.925) {
                this.monsterJump(state);
            }
        }
        return 0;
    };
    this.monsterJump = function (state) {
        var moved = false;
        var loopCount = 0;
        var range = state.int(state.rnd(2)) + 3;
        while (loopCount < 10 && !moved) {
            for (var xDelta =  -range; xDelta <= range && !moved; ++xDelta) {
                for (var yDelta = -range; yDelta <= range && !moved; ++yDelta) {
                    var xProbe = state.currentMonster.position.x + xDelta;
                    var yProbe = state.currentMonster.position.y + yDelta;
                    if (xProbe >=0 && xProbe < state.dungeon.size && yProbe >= 0 && yProbe < state.dungeon.size) {
                        if (!(xDelta >= -2 && xDelta <= 2 && yDelta >= -2 && yDelta <= 2)) {
                            if (state.rnd(1) < 0.7) {
                                if (state.dungeon.lookAt(xProbe,yProbe) === 0) {
                                    state.currentMonster.teleport(xProbe,yProbe,state.dungeon);
                                    moved = true;
                                }
                            }
                        }
                    }
                }
            }
            ++loopCount;
        }
    };
    this.spawnMonsters = function (state) {
        state.currentMonsters = [];
        state.monsterManual.monsters.forEach(function (item) {
            var child = item.spawn();
            state.currentMonsters.push(child);
            var counter = state.rnd(state.dungeon.size * state.dungeon.size);
            var xProbe = 0;
            var yProbe = 0;
            while (counter > 0) {
                if (state.dungeon.lookAt(xProbe, yProbe) === 0) {
                    --counter;
                } else {
                    xProbe = (1 + xProbe) % state.dungeon.size;
                    if (xProbe === 0) {
                        yProbe = (1 + yProbe) % state.dungeon.size;
                    }
                }
            }
            child.position = {x: xProbe, y: yProbe};
            console.log(child.name + " @ x:" + xProbe + ", y:" + yProbe);
        });
        if (state.currentMonsters.length > 0) {
            state.currentMonster = state.currentMonsters[0];
        } else {
            state.currentMonster = undefined;
        }
    };
};