function MachineState(stateId,
                      stateName,
                      stateFunction,
                      stateWaitForInput,
                      stateResultIds) {
    this.id = parseInt(stateId);
    this.name = stateName;
    this.action = stateFunction;
    this.waitForInput = (stateWaitForInput === "true");
    this.stateFunction = stateFunction;
    var results = [];
    stateResultIds.forEach(function (item) {
        results.push(parseInt(item));
    });
    this.results = results;
}