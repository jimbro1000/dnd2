describe('InputHandler', function () {
    it("should return an input buffer", function () {
        expect(new InputHandler()).toBeDefined();
    });
    describe('InputHandler Methods', function () {
        var inputHandler;
        beforeEach(function () {
            inputHandler = new InputHandler();
        });
        afterEach(function () {
            inputHandler.removeEventHooks();
        });
        it("should provide a method 'getNext'", function () {
            expect(inputHandler.getNext).toBeDefined();
        });
        it("should provide a method 'addNext'", function () {
            expect(inputHandler.addNext).toBeDefined();
        });
        it("should provide a method 'startReading'", function () {
            expect(inputHandler.startReading).toBeDefined();
        });
        it("should provide a method 'stopReading'", function () {
            expect(inputHandler.stopReading).toBeDefined();
        });
        it("should provide a method 'input'", function() {
            expect(inputHandler.input).toBeDefined();
        });
        describe('getNext', function () {
            it("should return null if the input buffer is empty", function () {
                expect(inputHandler.getNext()).toBeNull();
            });
            it("should return the first available key code captured", function () {
                inputHandler.addNext(1);
                expect(inputHandler.getNext()).toBe(1);
            })
        });
        describe('addNext', function () {
            it("should accept a key code and add that to the input buffer", function () {
                inputHandler.addNext(32);
                expect(inputHandler.inputBuffer[0]).toBe(32);
            });
            it("should accumulate key codes in the buffer", function () {
                inputHandler.addNext(32);
                inputHandler.addNext(33);
                expect(inputHandler.inputBuffer[1]).toBe(33);
            });
            it("should stop accumulating key codes if the buffer is full", function () {
                for (var i = 0; i <= inputHandler.BUFFER_LENGTH; ++i) {
                    inputHandler.addNext(i);
                }
                expect(inputHandler.inputBuffer[0]).toBe(0);
                expect(inputHandler.inputBuffer[inputHandler.BUFFER_LENGTH - 1]).toBe(inputHandler.BUFFER_LENGTH - 1);
            })
        });
        describe('startReading', function () {
            it("should allow keypress events to be captured", function () {
                inputHandler.startReading();
                mockKeyPress(49);
                expect(inputHandler.getNext()).toBe(49);
            });
        });
        describe('stopReading', function () {
            it("should prevent keypress events being captured", function () {
                inputHandler.startReading();
                mockKeyPress(49);
                inputHandler.stopReading();
                mockKeyPress(50);
                expect(inputHandler.getNext()).toBe(49);
            });
        });
        describe('input', function () {
            it("should accept a callback function as a parameter", function (done) {
                inputHandler.input().then(
                    function(output) { expect(output).toBeDefined(); done(); }
                    );
                mockKeyPress(13);
            });
            it("should return a string of the input received after receiving a 'return' key press", function (done) {
                inputHandler.input().then(
                    function(output) { expect(output).toBe("12"); done(); }
                );
                mockKeyPress(49);
                mockKeyPress(50);
                mockKeyPress(13);
            });
            it("should handle backspace key presses by adding code 8 to the buffer on key up", function () {
                inputHandler.startReading();
                mockKeyUp(8);
                expect(inputHandler.getNext()).toBe(8);
            });
            it("should accept a terminal object to echo output to", function (done) {
                var terminal = new Terminal("terminalConsole", 10, 20);
                inputHandler.input(terminal).then(
                    function(output) { expect(output).toBe("12"); done(); }
                );
                mockKeyPress(49);
                mockKeyPress(50);
                mockKeyPress(13);
                expect(terminal.grid[0].data).toContain("12");
                expect(terminal.grid[0].data).not.toContain("112");
            });
            it("should convert input to uppercase if inputConvert is 1", function (done) {
                var terminal = new Terminal("terminalConsole", 10, 20);
                inputHandler.inputConvert = inputHandler.INPUT_TO_UPPER;
                inputHandler.input(terminal).then(
                    function(output) { expect(output).toBe("AB"); done(); }
                );
                mockKeyPress(97, "a");
                mockKeyPress(66, "B");
                mockKeyPress(13);
            });
            it("should convert input to lowercase if inputConvert is 2", function (done) {
                var terminal = new Terminal("terminalConsole", 10, 20);
                inputHandler.inputConvert = inputHandler.INPUT_TO_LOWER;
                inputHandler.input(terminal).then(
                    function(output) { expect(output).toBe("ab"); done(); }
                );
                mockKeyPress(97, "a");
                mockKeyPress(66, "B");
                mockKeyPress(13);
            });
            it("should not modify input if inputConvert is 0", function (done) {
                var terminal = new Terminal("terminalConsole", 10, 20);
                inputHandler.inputConvert = inputHandler.INPUT_NATURAL;
                inputHandler.input(terminal).then(
                    function(output) { expect(output).toBe("aB"); done(); }
                );
                mockKeyPress(97, "a");
                mockKeyPress(66, "B");
                mockKeyPress(13);
            });
        });
    })
});

function mockKeyPress(code, char) {
    if (!char) {
        char = String.fromCharCode(code);
    }
    window.dispatchEvent(window.crossBrowser_initKeyboardEvent("keypress", {"key": String.fromCharCode(code), "char": char}));
}

function mockKeyUp(code, char) {
    if (!char) {
        char = String.fromCharCode(code);
    }
    window.dispatchEvent(window.crossBrowser_initKeyboardEvent("keyup", {"key": String.fromCharCode(code), "char": char}));
}