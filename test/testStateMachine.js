describe('StateMachine', function() {
    it("should return a new statemachine", function() {
        expect(new StateMachine()).toBeDefined();
    });
    describe('StateMachine methods', function() {
        var stateMachine;
        beforeEach(function () {
            stateMachine = new StateMachine();
        });
        it("should provide a method 'loadConfiguration'", function () {
            expect(stateMachine.loadConfiguration).toBeDefined();
        });
        it("should provide a method 'processConfiguration'", function () {
            expect(stateMachine.processConfiguration).toBeDefined();
        });
        it("should provide a method 'validateConfigurationStates'", function () {
            expect(stateMachine.validateConfigurationStates).toBeDefined();
        });
        it("should provide a method 'processState'", function () {
            expect(stateMachine.processState).toBeDefined();
        });
    });
    describe("loadConfiguration", function () {
        var stateMachine;
        beforeEach(function () {
            stateMachine = new StateMachine();
        });
        it("should expect a string as the name of a configuration as the first parameter and return a promise", function (done) {
            var failTest = function (error) {
                expect(error).toBeUndefined();
                done();
            };
            stateMachine.loadConfiguration("test.json")
                .then(
                    function (response) {
                        expect(response).toBeDefined();
                        done();
                    },
                    failTest
                );
        });
        it("should reject the promise if the configuration specified does not exist", function (done) {
            var failTest = function (response) {
                expect(response).toBeUndefined();
                done();
            };
            stateMachine.loadConfiguration("error.json")
                .then(
                    failTest,
                    function (error) {
                        expect(error).toBeDefined();
                        done();
                    });
        });
        it("should reject the promise if the parameters are invalid types", function (done) {
            var failTest = function (response) {
                expect(response).toBeUndefined();
                done();
            };
            stateMachine.loadConfiguration(1)
                .then(
                    failTest,
                    function (error) {
                        expect(error).toBeDefined();
                        done();
                    });
        });
    });
    describe("processConfiguration", function () {
        var stateMachine;
        var configurationObject;
        beforeEach(function (done) {
            stateMachine = new StateMachine();
            stateMachine.loadConfiguration("test.json")
                .then(
                    function (config) {
                        configurationObject = config;
                        done();
                    },
                    function (error) {
                        throw("unable to obtain test configuration: " + error);
                    }
                );
        });
        it("should accept a configuration object as the first parameter and convert it", function () {
            var result = stateMachine.processConfiguration(configurationObject);
            expect(result).toBe(true);
        });
        it("should override the name of the configuration if it is set in the object", function () {
            stateMachine.processConfiguration(configurationObject);
            expect(stateMachine.configurationName).toBe("test");
        });
        it("should create a state object for each state described in the configuration", function () {
            stateMachine.processConfiguration(configurationObject);
            expect(stateMachine.states).toBe(4);
        });
        it("should set the initial state to the value indicated when present", function () {
            stateMachine.processConfiguration(configurationObject);
            expect(stateMachine.initialState.id).toBe(1);
        });
        it("should default the initial state to the first state found if initial value not present", function (done) {
            stateMachine.loadConfiguration("testNoInitialState.json")
                .then(
                    function (config) {
                        stateMachine.processConfiguration(config);
                        expect(stateMachine.initialState.id).toBe(0);
                        done();
                    },
                    function (error) {
                        throw("unable to obtain test configuration: " + error);
                    }
                )
        });
    });
    describe("validateConfigurationStates", function () {
        var stateMachine;
        beforeEach(function () {
            stateMachine = new StateMachine();
        });
        it("should return true if configuration is complete", function (done) {
            stateMachine.loadConfiguration("test.json")
                .then(
                    function (config) {
                        stateMachine.processConfiguration(config);
                        expect(stateMachine.validateConfigurationStates()).toBe(true);
                        done();
                    },
                    function (error) {
                        throw("unable to obtain test configuration: " + error);
                    }
                );
        });
        it("should return false if configuration is incomplete", function (done) {
            stateMachine.loadConfiguration("testIncompleteStates.json")
                .then(
                    function (config) {
                        stateMachine.processConfiguration(config);
                        expect(stateMachine.validateConfigurationStates()).toBe(false);
                        done();
                    },
                    function (error) {
                        throw("unable to obtain test configuration: " + error);
                    }
                );
        });
    });
    describe("processState", function () {
        var stateMachine;
        beforeEach(function (done) {
            stateMachine = new StateMachine();
            stateMachine
                .loadConfiguration("test.json")
                .then(
                    function (config) {
                        stateMachine.processConfiguration(config);
                        if (stateMachine.validateConfigurationStates()) {
                            done();
                        } else {
                            throw("test configuration is invalid");
                        }
                    },
                    function (error) {
                        throw("unable to obtain test configuration: " + error);
                    }
                );
        });
        it("should process the current state to derive the next state and return 1 if there are more steps to run", function () {
            var result = stateMachine.processState();
            expect(result).toBe(1);
        });
        it("should process the current state and return 0 if a terminal state is achieved", function () {
            stateMachine.processState();
            var result = stateMachine.processState();
            expect(result).toBe(0);
        });
        it("should use the stateFunction object to provide functionality for the machine states", function () {
            stateMachine.currentState = 2;
            var result = stateMachine.processState();
            expect(result).toBe(1);
            result = stateMachine.processState();
            expect(result).toBe(1);
            result = stateMachine.processState();
            expect(result).toBe(0);
        });
        it("should suspend processing at the end of a function's operation if the waitForInput flag is set", function (done) {
            stateMachine.currentState = 3;
            spyOn(stateMachine.inputHandler, "input").and.callFake(function() {
                done();
            });
            stateMachine.processState();
            expect(stateMachine.inputHandler.input).toHaveBeenCalled();
        });
        // it("should resume processing at the end of a function's operation if the waitForInput flag is set and input is received", function () {
        //     stateMachine.currentState = 3;
        //     spyOn(stateMachine, "_inputCallback").and.callThrough();
        //     stateMachine.processState();
        //     expect(stateMachine._inputCallback).toHaveBeenCalled();
        // });
    });
});