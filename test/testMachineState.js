describe("MachineState", function () {
    it("should exist", function () {
        expect(MachineState).toBeDefined();
    });
    it("should accept state parameters id, name, action, wait and results", function () {
        var state = new MachineState(1, "state1", function () {
            return "action1";
        }, "true", [0, 1]);
        expect(state.id).toBe(1);
        expect(state.name).toBe("state1");
        expect(state.action()).toBe("action1");
        expect(state.waitForInput).toBe(true);
        expect(state.results[0]).toBe(0);
        expect(state.results[1]).toBe(1);
    });
});