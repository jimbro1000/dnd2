describe('Terminal', function () {
    it('should return terminal window object at creation', function () {
        expect(new Terminal("terminalConsole", 1, 1)).toBeDefined();
    });
    describe('constructor', function () {
        var terminal;
        beforeEach(function () {
            terminal = new Terminal("terminalConsole", 10, 20);
        });
        it('should accept two arguments for the rows and columns of the terminal', function () {
            expect(terminal.rows).toBe(10);
            expect(terminal.columns).toBe(20);
        });
        it('should accept a third argument for the DOM object that contains the terminal', function () {
            expect(terminal.elementId).toBe("terminalConsole");
        });
        it('should set the cursor position to 0, 0', function() {
            expect(terminal.cursorPosition.row).toBe(0);
            expect(terminal.cursorPosition.column).toBe(0);
        });
    });
    describe('cls', function () {
        var terminal;
        beforeEach(function () {
            terminal = new Terminal("terminalConsole", 10, 20);
        });
        it("should provide a function 'cls' to clear the terminal console", function () {
            expect(terminal.cls).toBeDefined();
        });
        it("should reset the cursor position to 0, 0", function() {
            expect(terminal.cursorPosition.row).toBe(0);
            expect(terminal.cursorPosition.column).toBe(0);
        });
    });
    describe('print', function () {
        var terminal;
        beforeEach(function () {
            terminal = new Terminal("terminalConsole", 10, 20);
            terminal.cls();
        });
        it("should provide a function 'print' to write text to the terminal console", function () {
            expect(terminal.print).toBeDefined();
        });
        it("should accept a value as a parameter and write the value as text to the terminal", function () {
            terminal.print("hello");
            expect(terminal.grid[0].data).toContain("hello");
        });
        it("should move the cursor to the next character past the end of the printed string", function () {
            terminal.print("hello world");
            expect(terminal.cursorPosition.row).toBe(0);
            expect(terminal.cursorPosition.column).toBe(11);
        });
        it("should move the cursor to the beginning of the next line if the text fits exactly to the terminal width", function () {
            terminal.print("The quick fox jumped");
            expect(terminal.cursorPosition.row).toBe(1);
            expect(terminal.cursorPosition.column).toBe(0);
        });
        it("should wrap text onto the next line if it overflows the current line", function () {
            terminal.print("The quick fox jumped over the lazy brown dog");
            expect(terminal.cursorPosition.row).toBe(2);
            expect(terminal.cursorPosition.column).toBe(4);
        });
        it("should scroll the terminal content if the printed text would overflow beyond the row limits of the terminal", function () {
            terminal.cursorPosition.row = 9;
            terminal.print("The quick fox jumped over the lazy brown dog");
            expect(terminal.cursorPosition.row).toBe(9);
            expect(terminal.cursorPosition.column).toBe(4);
        });
        it("should overwrite the existing terminal content without clearing to the end of the line", function () {
            terminal.print("Hello World!!");
            terminal.cursorPosition.column = 6;
            terminal.print("Julian");
            expect(terminal.grid[0].data.trim()).toBe("Hello Julian!");
        });
        it("should accept the tab character by translating into an appropriate number of spaces", function () {
            terminal.print("A" + terminal.TAB + "tab");
            expect(terminal.grid[0].data.trim()).toBe("A   tab");
        });
    });
    describe('println', function () {
        var terminal;
        beforeEach(function () {
            terminal = new Terminal("terminalConsole", 10, 20);
            terminal.cls();
        });
        it("should provide a function 'println' to write text to the terminal console", function () {
            expect(terminal.println).toBeDefined();
        });
        it("should accept a value as a parameter and write the value as text to the terminal", function () {
            terminal.println("hello");
            expect(terminal.grid[0].data).toContain("hello");
        });
        it("should move the cursor to the first character of the next row after the printed string", function () {
            terminal.println("hello world");
            expect(terminal.cursorPosition.row).toBe(1);
            expect(terminal.cursorPosition.column).toBe(0);
        });
        it("should wrap text onto the next line if it overflows the current line", function () {
            terminal.println("The quick fox jumped over the lazy brown dog");
            expect(terminal.cursorPosition.row).toBe(3);
            expect(terminal.cursorPosition.column).toBe(0);
        });
        it("should scroll the terminal content if the printed text would overflow beyond the row limits of the terminal", function () {
            terminal.cursorPosition.row = 9;
            terminal.println("The quick fox jumped over the lazy brown dog");
            expect(terminal.cursorPosition.row).toBe(9);
            expect(terminal.cursorPosition.column).toBe(0);
        });
        it("should overwrite the existing terminal content and clear to the end of the line", function () {
            terminal.print("Hello World!!");
            terminal.cursorPosition.column = 6;
            terminal.println("Julian");
            expect(terminal.grid[0].data.trim()).toBe("Hello Julian");
        });
        it("should accept the tab character by translating into an appropriate number of spaces", function () {
            terminal.println("A" + terminal.TAB + "tab");
            expect(terminal.grid[0].data.trim()).toBe("A   tab");
        });
    });
    describe('printc', function () {
        var terminal;
        beforeEach(function () {
            terminal = new Terminal("terminalConsole", 10, 20);
            terminal.cls();
        });
        it("should provide a function 'printc' to write text centred to the terminal console", function () {
            expect(terminal.printc).toBeDefined();
        });
        it("should accept a value as a parameter and write the value as text centered on the terminal", function () {
            terminal.printc("hello");
            expect(terminal.grid[0].data).toBe("       hello        ");
        });
        it("should move the cursor to the first character of the next row after the printed string", function () {
            terminal.printc("hello world");
            expect(terminal.cursorPosition.row).toBe(1);
            expect(terminal.cursorPosition.column).toBe(0);
        });
        it("should wrap text onto the next line if it overflows the current line", function () {
            terminal.printc("The quick fox jumped over the lazy brown dog");
            expect(terminal.cursorPosition.row).toBe(3);
            expect(terminal.cursorPosition.column).toBe(0);
        });
        it("should scroll the terminal content if the printed text would overflow beyond the row limits of the terminal", function () {
            terminal.cursorPosition.row = 9;
            terminal.printc("The quick fox jumped over the lazy brown dog");
            expect(terminal.cursorPosition.row).toBe(9);
            expect(terminal.cursorPosition.column).toBe(0);
        });
        it("should overwrite the existing terminal content and clear to the both sides of the line", function () {
            terminal.print("Hello World!!");
            terminal.printc("Julian");
            expect(terminal.grid[0].data.trim()).toBe("Julian");
        });
        it("should accept the tab character by translating into a single space", function () {
            terminal.printc("A" + terminal.TAB + "tab");
            expect(terminal.grid[0].data.trim()).toBe("A tab");
        });
    });
});